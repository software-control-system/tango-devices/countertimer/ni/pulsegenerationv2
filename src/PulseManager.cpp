//=============================================================================
// PulseManager.cpp
//=============================================================================
// abstraction.......PulseManager for PulseGenerationV2
// class.............PulseManager
// original author...S.Gara - Nexeya
//=============================================================================


// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "PulseManager.h"
#include <yat4tango/PropertyHelper.h>
#include <yat/utils/String.h>

namespace PulseGenerationV2_ns
{

	//- check pulse macro:
#define CHECK_PULSE_GENERATOR() \
	do \
	{ \
	if (! m_pulse) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("The pulse object isn't accessible "), \
	_CPTC("PulseManager::check_pulse_generator")); \
} while (0)

	//- check dyn attr manager macro:
#define CHECK_DYN_ATTR_MANAGER() \
	do \
	{ \
	if (! m_dyn_attr_manager)\
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("The dynamic attribute manager object isn't accessible "), \
	_CPTC("PulseManager::check_dyn_attr_manager")); \
} while (0)

	//- check start trigger macro:
#define CHECK_START_TRIGGER() \
	do \
	{ \
	if (! m_internal_start_trigger) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("The start trigger object isn't accessible "), \
	_CPTC("PulseManager::check_start_trigger")); \
} while (0)


// ============================================================================
// PulseManager::PulseManager ()
// ============================================================================ 
PulseManager::PulseManager (Tango::DeviceImpl * hostDevice)
: yat4tango::DeviceTask(hostDevice)
{
	//- trace/profile this method
	yat4tango::TraceHelper t("PulseManager::PulseManager", this);

	this->enable_timeout_msg(false);
	this->enable_periodic_msg(false);

  // Init member variables
	m_dyn_attr_manager = NULL;
  m_internal_start_trigger = NULL;
	m_pulse = NULL;
  m_state = Tango::INIT;
	m_status = "Initializing...";
}

// ============================================================================
// PulseManager::~PulseManager ()
// ============================================================================ 
PulseManager::~PulseManager ()
{
	this->enable_periodic_msg(false);

  if (this->m_internal_start_trigger)
  {
    delete this->m_internal_start_trigger;
    this->m_internal_start_trigger = NULL;
  }

	if (this->m_pulse)
	{
		delete m_pulse;
		m_pulse = NULL;
	}

	// remove dynamic attributes 
	if (m_dyn_attr_manager)
	{
		try
		{
			m_dyn_attr_manager->remove_attributes();
		}
		catch (...)
		{
			//- ignore any error
		}
	}

	// delete dynamic attributes manager
	if (m_dyn_attr_manager)
	{
		delete m_dyn_attr_manager;
		m_dyn_attr_manager = NULL;
	}
}

// ============================================================================
// PulseManager::get_state ()
// ============================================================================ 
Tango::DevState PulseManager::get_state()
{
	return m_state;
}

// ============================================================================
// PulseManager::get_status ()
// ============================================================================ 
std::string PulseManager::get_status()
{
	return m_status;
}

// ============================================================================
// PulseManager::update_state_status ()
// ============================================================================ 
void PulseManager::update_state_status() 
  throw (Tango::DevFailed)
{
	CHECK_PULSE_GENERATOR();

  // Get pulse generator current state & status
	m_state = m_pulse->get_state();
	m_status = m_pulse->get_status();
}

// ============================================================================
// PulseManager::init ()
// ============================================================================ 
void PulseManager::init(PulseGenerationV2Config& cfg)
  throw (Tango::DevFailed)
{
	m_cfg = cfg;

  // Create dynamic attribute manager
	try
	{
    this->m_dyn_attr_manager = new yat4tango::DynamicAttributeManager(this->m_cfg.hostDevice);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to create Dynamic Attribute Manager"), 
			_CPTC("PulseManager::init")); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to create Dynamic Attribute Manager" << std::endl;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to create Dynamic Attribute Manager"), 
			_CPTC("PulseManager::init")); 
	}

	if (!this->m_dyn_attr_manager)
	{
		ERROR_STREAM << "Failed to access dynamic attributes manager" << std::endl;
		THROW_DEVFAILED(
      _CPTC("HARDWARE_FAILURE"), 
			_CPTC("Failed to access dynamic attributes manager"), 
			_CPTC("PulseManager::init")); 
	} 

	try
	{
    // create new pulse generator
		this->manage_pulse();
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Initialization failed - failed to create pulse object."), 
			_CPTC("PulseManager::init")); 
	}
	catch (...)
	{
		ERROR_STREAM << "Initialization failed - failed to create pulse object." << std::endl;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Initialization failed - failed to create pulse object."), 
			_CPTC("PulseManager::init")); 
	}

  // create dynamic attributes
	if (!create_dyn_attrs())
	{
		ERROR_STREAM << "Failed to create scalar dynamic attributes!" << std::endl;
		THROW_DEVFAILED(
      _CPTC("HARDWARE_FAILURE"), 
			_CPTC("Failed to create scalar dynamic attributes!"), 
			_CPTC("PulseManager::init")); 
	} 

	INFO_STREAM << "PulseManager::init OK" << endl;
	m_state = Tango::STANDBY;
	m_status = "Device is up and ready";
}

// ============================================================================
// PulseManager::process_message
// ============================================================================
void PulseManager::process_message (yat::Message& msg)
  throw (Tango::DevFailed)
{
	//- handle msg
	switch (msg.type())
	{
		//- THREAD_INIT ----------------------
	case yat::TASK_INIT:
		{
			DEBUG_STREAM << "PulseManager::handle_message::THREAD_INIT::thread is starting up" << std::endl;
			this->init_i();
		} 
		break;

		//- THREAD_EXIT ----------------------
	case yat::TASK_EXIT:
		{
			DEBUG_STREAM << "PulseManager::handle_message::THREAD_EXIT::thread is quitting" << std::endl;
		}
		break;

		//- THREAD_PERIODIC ------------------
	case yat::TASK_PERIODIC:
		{
			//DEBUG_STREAM << "PulseManager::handle_message::THREAD_PERIODIC" << std::endl;
			this->periodic_job_i();
		}
		break;

		//- THREAD_TIMEOUT -------------------
	case yat::TASK_TIMEOUT:
		{
			//- not used in this device
		}
		break;
		//- UNHANDLED MSG --------------------
	default:
		DEBUG_STREAM << "PulseManager::handle_message::unhanded msg type received" << std::endl;
		break;
	}
}

// ============================================================================
// PulseManager::start ()
// ============================================================================ 
void PulseManager::start()
  throw (Tango::DevFailed)
{
	CHECK_PULSE_GENERATOR();

  DEBUG_STREAM << "PulseManager::start() entering..." << std::endl;

  // start the pulse generation 
  // => wait for (internal or external) start trigger to really generate the 
  // specified pulse train.
	try
	{
		m_pulse->start();
	}
	catch (Tango::DevFailed & df)
	{
		ERROR_STREAM << df << std::endl;
		RETHROW_DEVFAILED(df,_CPTC("DEVICE_ERROR"), 
			_CPTC("Start failed"), 
			_CPTC("PulseManager::start")); 

	}
	catch (...)
	{
		ERROR_STREAM << "failed to Start" << std::endl;
		THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
			_CPTC("Start failed"), 
			_CPTC("PulseManager::start")); 
	}

  // Start the internal start trigger if exists
  if (this->m_internal_start_trigger)
  {
		try
		{
			this->m_internal_start_trigger->start();
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, 
        _CPTC("DEVICE_ERROR"), 
				_CPTC("Failed to start internal start trigger object."), 
				_CPTC("PulseManager::start")); 
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to start internal start trigger object." << std::endl;
			THROW_DEVFAILED(
        _CPTC("DEVICE_ERROR"), 
				_CPTC("Failed to start internal start trigger object."), 
				_CPTC("PulseManager::start")); 
		}
  }
}

// ============================================================================
// PulseManager::stop ()
// ============================================================================ 
void PulseManager::stop()
  throw (Tango::DevFailed)
{
  // Stop internal start trigger if exists
  if (this->m_internal_start_trigger)
  {
		try
		{
			this->m_internal_start_trigger->stop();
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, 
        _CPTC("DEVICE_ERROR"), 
				_CPTC("Failed to stop internal start trigger object."), 
				_CPTC("PulseManager::stop")); 
		}
		catch (...)
		{
			ERROR_STREAM << "Failed to stop internal start trigger object." << std::endl;
			THROW_DEVFAILED(
        _CPTC("DEVICE_ERROR"), 
				_CPTC("Failed to stop internal start trigger object."), 
				_CPTC("PulseManager::stop")); 
		}
  }

	CHECK_PULSE_GENERATOR();

  // stop the pulse generation
	try
	{
		m_pulse->stop();
	}
	catch (Tango::DevFailed & df)
	{
		ERROR_STREAM << df << std::endl;
		RETHROW_DEVFAILED(df,_CPTC("DEVICE_ERROR"), 
			_CPTC("Stop failed"), 
			_CPTC("PulseManager::stop")); 
	}
	catch (...)
	{
		ERROR_STREAM << "failed to Stop" << std::endl;
		THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
			_CPTC("Stop failed"), 
			_CPTC("PulseManager::stop")); 
	}
}

// ============================================================================
// PulseManager::resetBoard ()
// ============================================================================ 
void PulseManager::resetBoard()
  throw (Tango::DevFailed)
{
	CHECK_PULSE_GENERATOR();

  // reset board
	try
	{
		m_pulse->resetBoard();
	}
	catch (Tango::DevFailed & df)
	{
		ERROR_STREAM << df << std::endl;
		RETHROW_DEVFAILED(df,_CPTC("DEVICE_ERROR"), 
			_CPTC("ResetBoard failed"), 
			_CPTC("PulseManager::resetBoard")); 
	}
	catch (...)
	{
		ERROR_STREAM << "failed to ResetBoard" << std::endl;
		THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
			_CPTC("ResetBoard failed"), 
			_CPTC("PulseManager::resetBoard")); 
	}
}

// ============================================================================
// PulseManager::getDriverVersion ()
// ============================================================================ 
std::string PulseManager::getDriverVersion()
  throw (Tango::DevFailed)
{
	CHECK_PULSE_GENERATOR();

	std::string l_driver_version;
  // get driver version from baord
	try
	{
		l_driver_version = m_pulse->getDriverVersion();
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to get driver version"), 
			_CPTC("PulseManager::getDriverVersion")); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to get driver version" << std::endl;
		THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to get driver version"), 
			_CPTC("PulseManager::getDriverVersion")); 
	}

	return l_driver_version;
}

// ============================================================================
// PulseManager::updateGenerationType ()
// ============================================================================ 
void PulseManager::updateGenerationType(std::string p_generation)
  throw (Tango::DevFailed)
{
	GENERATION_TYPE l_generation_type;

  // format new generation type
	if (p_generation.compare(GT_CONTINUOUS_STR) == 0)
	{
		l_generation_type = GT_CONTINUOUS;
	}
	else if (p_generation.compare(GT_FINITE_STR) == 0)
	{
		l_generation_type = GT_FINITE;
	}
	else if (p_generation.compare(GT_RETRIG_STR) == 0)
	{
		l_generation_type = GT_RETRIG;
	}
	else 
	{
		THROW_DEVFAILED(
      _CPTC("SOFTWARE_FAILURE"), 
			_CPTC("The value for generationType is not valid. Use CONTINUOUS, FINITE or RETRIG"), 
			_CPTC("PulseManager::updateGenerationType")); 	
	}

  // check if equals current generation type
	if (l_generation_type != m_cfg.generationType)
	{
    // store current generation type in case of error
		GENERATION_TYPE l_old_generation_type = m_cfg.generationType;
		m_cfg.generationType = l_generation_type;

		try
		{
      // delete current pulse generator & create new one
			this->manage_pulse();
		}
		catch (Tango::DevFailed &e)
		{
			// recreate old pulse
			m_cfg.generationType = l_old_generation_type;
      this->manage_pulse();

			ERROR_STREAM << e << std::endl;
      RETHROW_DEVFAILED(e, _CPTC("DEVICE_ERROR"), 
				_CPTC("initialization failed - failed to create pulse"), 
				_CPTC("PulseManager::updateGenerationType")); 
		}
		catch (...)
		{
      // recreate old pulse
			m_cfg.generationType = l_old_generation_type;
      this->manage_pulse();
			
			ERROR_STREAM << "initialization failed - failed to create pulse" << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
				_CPTC("initialization failed - failed to create pulse"), 
				_CPTC("PulseManager::updateGenerationType")); 
		}

		// add/remove the pulseNb attribute if necessary
    bool l_set_pulses_nb = this->m_pulse->isSetPulseNeeded();

		try
		{
			CHECK_DYN_ATTR_MANAGER();

			m_dyn_attr_manager->get_attribute(PULSE_NB);
			
      // atribute is already present
			if (!l_set_pulses_nb)
			{
				this->remove_attr_pulse_nb();
			}
		}
		catch(...)
		{
			// attribute is not present 
			if (l_set_pulses_nb)
			{
				this->add_attr_pulse_nb();
			}
		}
	}
}

// ============================================================================
// PulseManager::updateStartTrgUse ()
// ============================================================================ 
void PulseManager::updateStartTrgUse(bool p_ext_trg, std::string p_ext_src)
  throw (Tango::DevFailed)
{
  // check if equals current external start trigger value
  if (p_ext_trg != m_cfg.pulses.triggerData.external)
  {
    // store current value in case of error
    bool l_old_trg = m_cfg.pulses.triggerData.external;
    std::string l_old_src = m_cfg.pulses.triggerData.source;
    m_cfg.pulses.triggerData.external = p_ext_trg;
    m_cfg.pulses.triggerData.source = p_ext_src;

    try
    {
      // delete current pulse generator & create new one
      this->manage_pulse();
    }
    catch (Tango::DevFailed &e)
    {
      // recreate old pulse
      m_cfg.pulses.triggerData.external = l_old_trg;
      m_cfg.pulses.triggerData.source = l_old_src;
      this->manage_pulse();

      ERROR_STREAM << e << std::endl;
      RETHROW_DEVFAILED(e, _CPTC("DEVICE_ERROR"), 
				_CPTC("initialization failed - failed to create pulse"), 
				_CPTC("PulseManager::updateStartTrgUse")); 
    }
    catch (...)
    {
      // recreate old pulse
      m_cfg.pulses.triggerData.external = l_old_trg;
      m_cfg.pulses.triggerData.source = l_old_src;
      this->manage_pulse();
			
      ERROR_STREAM << "initialization failed - failed to create pulse" << std::endl;
      THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
				_CPTC("initialization failed - failed to create pulse"), 
				_CPTC("PulseManager::updateStartTrgUse")); 
    }
  }
}

// ============================================================================
// PulseManager::manage_pulse ()
// ============================================================================ 
void PulseManager::manage_pulse()
  throw (Tango::DevFailed)
{
  // Delete current pulse generator if exists
	if (this->m_pulse)
	{
		try
		{
			this->m_pulse->release();
	    delete this->m_pulse;
	    this->m_pulse = NULL;
		}
		catch (...)
		{
			//ignore
		}
	}

  // Delete current internal start trigger if exists
	if (this->m_internal_start_trigger)
	{
		try
		{
			this->m_internal_start_trigger->release();
		  delete this->m_internal_start_trigger;
		  this->m_internal_start_trigger = NULL;		}
		catch (...)
		{
			//ignore
			this->m_internal_start_trigger = NULL;		}
	}

  // Create new internal start trigger if needed,
  // i.e. we don't use external start trigger.
  // In this case, the device has to create its own start trigger in order
  // to synchronize the generation on all channels.
  if (!m_cfg.pulses.triggerData.external)
  {
    DEBUG_STREAM << "Create internal starter object... " << std::endl;
    // Instanciate internal start trigger object
    try
		{
			this->m_internal_start_trigger = PulseFactory::instanciate(m_cfg.hostDevice, m_cfg.boardType);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, 
        _CPTC("DEVICE_ERROR"), 
				_CPTC("Initialization failed - failed to create internal start trigger object."), 
				_CPTC("PulseManager::manage_pulse")); 
		}
		catch (...)
		{
			ERROR_STREAM << "Initialization failed - failed to create internal start trigger object." << std::endl;
			THROW_DEVFAILED(
        _CPTC("DEVICE_ERROR"), 
				_CPTC("Initialization failed - failed to create internal start trigger object."), 
				_CPTC("PulseManager::manage_pulse")); 
		}

		if (!m_internal_start_trigger)
		{
			ERROR_STREAM << "Initialization failed - failed to access internal start trigger object." << std::endl;
			THROW_DEVFAILED(
        _CPTC("DEVICE_ERROR"), 
				_CPTC("Initialization failed - failed to access internal start trigger object."), 
				_CPTC("PulseManager::manage_pulse")); 
		}

    // Initialize internal start trigger object
		try
		{
			this->m_internal_start_trigger->init(m_cfg.boardId);
		}
		catch (Tango::DevFailed &e)
		{
			ERROR_STREAM << e << std::endl;
			RETHROW_DEVFAILED(e, 
        _CPTC("DEVICE_ERROR"), 
				_CPTC("Initialization failed - failed to init internal start trigger."), 
				_CPTC("PulseManager::manage_pulse")); 
		}
		catch (...)
		{
			ERROR_STREAM << "Initialization failed - failed to init internal start trigger." << std::endl;
			THROW_DEVFAILED(
        _CPTC("DEVICE_ERROR"), 
				_CPTC("Initialization failed - failed to init internal start trigger."), 
				_CPTC("PulseManager::manage_pulse")); 
		}
  }

	// Instanciate new pulse generator
	try
	{
		this->m_pulse = PulseFactory::instanciate(m_cfg.hostDevice, m_cfg.boardType, m_cfg.generationType);	
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Initialization failed - failed to create pulse object."), 
			_CPTC("PulseManager::manage_pulse")); 
	}
	catch (...)
	{
		ERROR_STREAM << "initialization failed - failed to create pulse object." << std::endl;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Initialization failed - failed to create pulse object."), 
			_CPTC("PulseManager::manage_pulse")); 
	}

	if (!m_pulse)
	{
		ERROR_STREAM << "Initialization failed - failed to access pulse object." << std::endl;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Initialization failed - failed to access pulse object."), 
			_CPTC("PulseManager::manage_pulse")); 
	}
	
  // Init pulse generator
  try
  {
    // Get internal start trigger infos from start trigger object
    // to initialize pulse config (for external trigger, these informations
    // are set in init_device function).
    if (this->m_internal_start_trigger)
    {
      this->m_cfg.pulses.triggerData = 
        this->m_internal_start_trigger->getTriggerData();
    }

    this->m_pulse->initConfig(m_cfg.pulses);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		RETHROW_DEVFAILED(e, 
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Initialization failed - failed to init counters."), 
			_CPTC("PulseManager::manage_pulse")); 
	}
	catch (...)
	{
		ERROR_STREAM << "Initialization failed - failed to init counters." << std::endl;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Initialization failed - failed to init counters."), 
			_CPTC("PulseManager::manage_pulse")); 
	}
}

// ============================================================================
// PulseManager::add_attr_pulse_nb ()
// ============================================================================ 
void PulseManager::add_attr_pulse_nb()
  throw (Tango::DevFailed)
{
  // define dynamic attribute for:
  
  //pulse number
	yat4tango::DynamicAttributeInfo dai;
	dai.dev = m_cfg.hostDevice;
	dai.tai.name = PULSE_NB;
	dai.tai.label = PULSE_NB;
	//- describe the dyn attr we want...
	dai.tai.data_type = Tango::DEV_LONG;
	dai.tai.data_format = Tango::SCALAR;
	dai.tai.writable = Tango::READ_WRITE;
	dai.tai.disp_level = Tango::OPERATOR;
	dai.tai.description = "Number of pulses. Available only in RETRIG and FINITE modes";
	//- attribute properties:
	dai.tai.unit = "";
	dai.tai.standard_unit = "";
	dai.tai.display_unit = "";
	dai.tai.format = "%d";
    dai.tai.min_value = "1";

	//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
	dai.cdb = false;
	//- read callback
	dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
		&PulseManager::read_pulse_number);
	//- write callback
	dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
		&PulseManager::write_pulse_number);

	CHECK_DYN_ATTR_MANAGER();
	
  m_dyn_attr_manager->add_attribute(dai);
}

// ============================================================================
// PulseManager::remove_attr_pulse_nb ()
// ============================================================================ 
void PulseManager::remove_attr_pulse_nb()
  throw (Tango::DevFailed)
{
  // remove pulse number dynamic attribute
	m_dyn_attr_manager->remove_attribute(PULSE_NB);
}

// ============================================================================
// PulseManager::periodic_job_i ()
// ============================================================================ 
void PulseManager::periodic_job_i()
  throw (Tango::DevFailed)
{
	update_state_status();
}

// ============================================================================
// PulseManager::init_i ()
// ============================================================================ 
void PulseManager::init_i()
{
	this->set_periodic_msg_period(50);
	this->enable_periodic_msg(true);
}

// ============================================================================
// PulseManager::create_dyn_attrs ()
// ============================================================================ 
bool PulseManager::create_dyn_attrs() 
  throw (Tango::DevFailed)
{
  // create dynamic attributes for:
  
  //-- defined counters
	bool l_setStateToFault = false;

	try
	{
		DEBUG_STREAM << "PulseManager::create_dyn_attrs" << std::endl;
		size_t nbChan2Parse = m_cfg.pulses.channels.size();

		std::vector<yat4tango::DynamicAttributeInfo> dai(nbChan2Parse * 5);

		CHECK_DYN_ATTR_MANAGER();

		DEBUG_STREAM << "PulseManager::create_dyn_attrs nbChan2Parse = " << nbChan2Parse << std::endl;
		unsigned int idx = 0;

		for (ChannelDef_it_t it = m_cfg.pulses.channels.begin() ; 
         it != m_cfg.pulses.channels.end(); ++it)
		{
      unsigned int l_id = it->second.id;
      std::string attrDescription;
      std::string id_str = yat::XString<unsigned int>::to_string(l_id);
			
      //enabled attribute
      attrDescription = "Enable/disable counter";
			dai[idx].dev = m_cfg.hostDevice;
			dai[idx].tai.name = COUNTER_CH + id_str + ENABLED_CH;
			dai[idx].tai.writable = Tango::READ_WRITE;
			dai[idx].tai.data_format = Tango::SCALAR;
			dai[idx].tai.data_type = Tango::DEV_BOOLEAN;
			dai[idx].tai.disp_level = Tango::OPERATOR;
			dai[idx].tai.description = attrDescription;
			//dai[idx].memorized = true; // to be enabled with tango 8
			dai[idx].rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&PulseManager::scalar_attr_enabled_rd_cb);
			dai[idx].wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&PulseManager::scalar_attr_enabled_wr_cb);
			dai[idx].set_user_data(l_id);

			// idleState Attribute
			attrDescription = "Initial level (LOW or HIGH) of the generated signal";
			dai[idx+1].dev = m_cfg.hostDevice;
			dai[idx+1].tai.name = IDLE_STATE_CH + id_str;
			dai[idx+1].tai.writable = Tango::READ_WRITE;
			dai[idx+1].tai.data_format = Tango::SCALAR;
			dai[idx+1].tai.format = "";
			dai[idx+1].tai.data_type = Tango::DEV_STRING;
			dai[idx+1].tai.disp_level = Tango::OPERATOR;
			dai[idx+1].tai.description = attrDescription;
			dai[idx+1].tai.unit = " ";
      dai[idx+1].tai.min_value = "";
			//dai[idx+1].memorized = true; // to be enabled with tango 8
			dai[idx+1].rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&PulseManager::scalar_attr_idle_rd_cb);
			dai[idx+1].wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&PulseManager::scalar_attr_idle_wr_cb);
			dai[idx+1].set_user_data(l_id);

			// initialDelay Attribute
			attrDescription = "Delay from the start of the generation till the beginning of the signal";
			dai[idx+2].dev = m_cfg.hostDevice;
			dai[idx+2].tai.name = INITIAL_DELAY_CH + id_str;
			dai[idx+2].tai.writable = Tango::READ_WRITE;
			dai[idx+2].tai.data_format = Tango::SCALAR;
			dai[idx+2].tai.format = "%6.2f";
			dai[idx+2].tai.data_type = Tango::DEV_DOUBLE;
			dai[idx+2].tai.disp_level = Tango::OPERATOR;
			dai[idx+2].tai.description = attrDescription;
			dai[idx+2].tai.unit = "ms";
      dai[idx+2].tai.min_value = "0.0";
			//dai[idx+2].memorized = true; // to be enabled with tango 8
			dai[idx+2].rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&PulseManager::scalar_attr_initial_rd_cb);
			dai[idx+2].wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&PulseManager::scalar_attr_initial_wr_cb);
			dai[idx+2].set_user_data(l_id);

			// lowDelay Attribute
			attrDescription = "If idleStateCounter is LOW, it corresponds to High delay. If idleStateCounter is HIGH, it corresponds to Low delay";
			dai[idx+3].dev = m_cfg.hostDevice;
			dai[idx+3].tai.name = LOW_DELAY_CH + id_str;
			dai[idx+3].tai.writable = Tango::READ_WRITE;
			dai[idx+3].tai.data_format = Tango::SCALAR;
			dai[idx+3].tai.format = "%6.2f";
			dai[idx+3].tai.data_type = Tango::DEV_DOUBLE;
			dai[idx+3].tai.disp_level = Tango::OPERATOR;
			dai[idx+3].tai.description = attrDescription;
			dai[idx+3].tai.unit = "ms";
      dai[idx+3].tai.min_value = "0.0"; 
			//dai[idx+3].memorized = true;  // to be enabled with tango 8
			dai[idx+3].rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&PulseManager::scalar_attr_low_rd_cb);
			dai[idx+3].wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&PulseManager::scalar_attr_low_wr_cb);
			dai[idx+3].set_user_data(l_id);

			// pulseWidth Attribute
			attrDescription = "If idleStateCounter is LOW, it corresponds to Low delay. If idleStateCounter is HIGH, it corresponds to High delay";
			dai[idx+4].dev = m_cfg.hostDevice;
			dai[idx+4].tai.name = PULSE_WIDTH_CH + id_str;
			dai[idx+4].tai.writable = Tango::READ_WRITE;
			dai[idx+4].tai.data_format = Tango::SCALAR;
			dai[idx+4].tai.format = "%6.2f";
			dai[idx+4].tai.data_type = Tango::DEV_DOUBLE;
			dai[idx+4].tai.disp_level = Tango::OPERATOR;
			dai[idx+4].tai.description = attrDescription;
			dai[idx+4].tai.unit = "ms";
      dai[idx+4].tai.min_value = "0.0"; 
			//dai[idx+4].memorized = true;  // to be enabled with tango 8
			dai[idx+4].rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, 
				&PulseManager::scalar_attr_pulse_rd_cb);
			dai[idx+4].wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, 
				&PulseManager::scalar_attr_pulse_wr_cb);
			dai[idx+4].set_user_data(l_id);
			
      idx+=5;
		}

		//- add all dynamic attributes to the device interface
		this->m_dyn_attr_manager->add_attributes(dai);
	}
	catch( Tango::DevFailed &df )
	{
		ERROR_STREAM << "Scalar dynamic attribute creation failed: " << df << std::endl;
		l_setStateToFault = true;
		m_status = "Scalar dynamic attribute creation failed: see logs for details";
	}
	catch(...)
	{
		ERROR_STREAM << "Scalar dynamic attribute creation failed: Unknown exception caught"<< std::endl;
		l_setStateToFault = true;
		m_status =  "Scalar dynamic attribute creation failed: Unknown exception caught";
	}
	
  //-- for nb pulse (if necessary)
  bool l_set_pulses_nb = m_pulse->isSetPulseNeeded();

	// add/remove the pulseNb attribute
	try
	{
		m_dyn_attr_manager->get_attribute(PULSE_NB);
		//atribute is already present
		if (!l_set_pulses_nb)
		{
			remove_attr_pulse_nb();
		}
	}
	catch(...)
	{
		// attribute is not present 
		if (l_set_pulses_nb)
		{
			add_attr_pulse_nb();
		}
	}


	return !l_setStateToFault;
}

// ============================================================================
// PulseManager::read_pulse_number ()
// ============================================================================ 
void PulseManager::read_pulse_number(yat4tango::DynamicAttributeReadCallbackData & cbd)
{
	static Tango::DevLong l_attr;
	l_attr = static_cast<Tango::DevLong>(m_cfg.pulses.nbPulses);
	cbd.tga->set_value(&l_attr);
}

// ============================================================================
// PulseManager::write_pulse_number ()
// ============================================================================ 
void PulseManager::write_pulse_number(yat4tango::DynamicAttributeWriteCallbackData & cbd)
{
	CHECK_PULSE_GENERATOR();

  // check device state
	if (m_state == Tango::RUNNING)
	{
		std::string l_msg = "Writing pulse number is not allowed in RUNNING state.";
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC(l_msg.c_str()), 
			_CPTC("PulseGenerationV2::write_pulse_number")); 
	}

	Tango::DevLong l_val;
	cbd.tga->get_write_value(l_val);

  // store current value in case of error
	int l_old_val = m_cfg.pulses.nbPulses;

	//- reconfigure the board
	m_cfg.pulses.nbPulses = l_val;
	try
	{
		m_pulse->initConfig(m_cfg.pulses);
    
    // memorize new value
    std::string prop_name = std::string("__") + PULSE_NB;
    yat4tango::PropertyHelper::set_property<Tango::DevLong>(this->m_cfg.hostDevice, prop_name, m_cfg.pulses.nbPulses);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		m_cfg.pulses.nbPulses = l_old_val;
		RETHROW_DEVFAILED(e, 
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Pulse number update error! Failed to update counters!"), 
			_CPTC("PulseManager::write_pulse_number")); 
	}
	catch (...)
	{
		ERROR_STREAM << "Pulse number update error! Failed to update counters!" << std::endl;
		m_cfg.pulses.nbPulses = l_old_val;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Pulse number update error! Failed to update counters!"), 
			_CPTC("PulseManager::write_pulse_number")); 
	}
}

//============================================================
//PulseManager::scalar_attr_enabled_rd_cb
//============================================================
void PulseManager::scalar_attr_enabled_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
  throw (Tango::DevFailed)
{
  // read callback for 'enabled' attribute

	std::string attrName = d.tga->get_name();

	unsigned int l_id = 0;
	d.dya->get_user_data<unsigned int>(l_id);

	static Tango::DevBoolean l_attr;
	l_attr = static_cast<Tango::DevBoolean>(m_cfg.pulses.channels[l_id].enabled);

	d.tga->set_value(&l_attr);
}

//============================================================
//PulseManager::scalar_attr_idle_rd_cb
//============================================================
void PulseManager::scalar_attr_idle_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
  throw (Tango::DevFailed)
{
  // read callback for 'idle state' attribute

	std::string attrName = d.tga->get_name();

	unsigned int l_id = 0;
  static char * l_attr;
	d.dya->get_user_data<unsigned int>(l_id);

	if (m_cfg.pulses.channels[l_id].enabled)
	{
    l_attr = const_cast<char*>(m_cfg.pulses.channels[l_id].idleState.c_str());
		d.tga->set_value(&l_attr);
	}
	else
	{
    std::string inv = "invalid";
    l_attr = const_cast<char*>(inv.c_str());
    d.tga->set_value(&l_attr);
		d.tga->set_quality(Tango::ATTR_INVALID);
	}
}

//============================================================
//PulseManager::scalar_attr_initial_rd_cb
//============================================================
void PulseManager::scalar_attr_initial_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
  throw (Tango::DevFailed)
{
  // read callback for 'initial delay' attribute

	std::string attrName = d.tga->get_name();

	unsigned int l_id = 0;
	d.dya->get_user_data<unsigned int>(l_id);

	if (m_cfg.pulses.channels[l_id].enabled)
	{
		static Tango::DevDouble l_attr;
		l_attr = static_cast<Tango::DevDouble>(m_cfg.pulses.channels[l_id].initialDelay);
		d.tga->set_value(&l_attr);
	}
	else
	{
		d.tga->set_quality(Tango::ATTR_INVALID);
	}
}

//============================================================
//PulseManager::scalar_attr_low_rd_cb
//============================================================
void PulseManager::scalar_attr_low_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
  throw (Tango::DevFailed)
{
  // read callback for 'low delay' attribute

	std::string attrName = d.tga->get_name();

	unsigned int l_id = 0;
	d.dya->get_user_data<unsigned int>(l_id);

	if (m_cfg.pulses.channels[l_id].enabled)
	{
		static Tango::DevDouble l_attr;
		l_attr = static_cast<Tango::DevDouble>(m_cfg.pulses.channels[l_id].lowDelay);
		d.tga->set_value(&l_attr);
	}
	else
	{
		d.tga->set_quality(Tango::ATTR_INVALID);
	}
}

//============================================================
//PulseManager::scalar_attr_pulse_rd_cb
//============================================================
void PulseManager::scalar_attr_pulse_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
  throw (Tango::DevFailed)
{
  // read callback for 'pulse width' attribute

	std::string attrName = d.tga->get_name();

	unsigned int l_id = 0;
	d.dya->get_user_data<unsigned int>(l_id);

	if (m_cfg.pulses.channels[l_id].enabled)
	{
		static Tango::DevDouble l_attr;
		l_attr = static_cast<Tango::DevDouble>(m_cfg.pulses.channels[l_id].highDelay);
		d.tga->set_value(&l_attr);
	}
	else
	{
		d.tga->set_quality(Tango::ATTR_INVALID);
	}
}

//============================================================
//PulseManager::scalar_attr_enabled_wr_cb
//============================================================
void PulseManager::scalar_attr_enabled_wr_cb (yat4tango::DynamicAttributeWriteCallbackData& d)
  throw (Tango::DevFailed)
{
  // write callback for 'enabled' attribute

	CHECK_PULSE_GENERATOR();

	std::string attrName = d.tga->get_name();

  // check device state
	if (m_state == Tango::RUNNING)
	{
		std::string l_msg = "Writing ";
		l_msg += attrName;
		l_msg += " is not allowed in RUNNING state.";
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC(l_msg.c_str()), 
			_CPTC("PulseGenerationV2::scalar_attr_enabled_wr_cb")); 
	}

  // store current value in case of error
	bool l_old_val;
	unsigned int l_id = 0;

	try
	{
		d.dya->get_user_data<unsigned int>(l_id);

		l_old_val = m_cfg.pulses.channels[l_id].enabled;
		
    Tango::DevBoolean l_val;
		d.tga->get_write_value(l_val);
		m_cfg.pulses.channels[l_id].enabled = l_val;

		m_pulse->initConfig(m_cfg.pulses);

    // memorize new value
    std::string prop_name = std::string("__") + attrName;
    yat4tango::PropertyHelper::set_property<bool>(this->m_cfg.hostDevice, prop_name, l_val);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		m_cfg.pulses.channels[l_id].enabled = l_old_val;
		RETHROW_DEVFAILED(e, 
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Enabling counter error! Failed to update counter!"), 
			_CPTC("PulseManager::scalar_attr_enabled_wr_cb")); 
	}
	catch (...)
	{
		ERROR_STREAM << "Enabling counter error! Failed to update counter!" << std::endl;
		m_cfg.pulses.channels[l_id].enabled = l_old_val;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Enabling counter error! Failed to update counter!"), 
			_CPTC("PulseManager::scalar_attr_enabled_wr_cb")); 
	}
}

//============================================================
//PulseManager::scalar_attr_idle_wr_cb
//============================================================
void PulseManager::scalar_attr_idle_wr_cb (yat4tango::DynamicAttributeWriteCallbackData& d)
  throw (Tango::DevFailed)
{
  // write callback for 'idle state' attribute

	CHECK_PULSE_GENERATOR();

	std::string attrName = d.tga->get_name();

  // check device state
	if (m_state == Tango::RUNNING)
	{
		std::string l_msg = "Writing ";
		l_msg += attrName;
		l_msg += " is not allowed in RUNNING state.";
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC(l_msg.c_str()), 
			_CPTC("PulseGenerationV2::scalar_attr_idle_wr_cb")); 
	}

  std::string l_old_val;
  unsigned int l_id = 0;

  try
	{
	  d.dya->get_user_data<unsigned int>(l_id);

    // store current value in case of error
    l_old_val = m_cfg.pulses.channels[l_id].idleState;
    
    if (m_cfg.pulses.channels[l_id].enabled)
    {
		  Tango::DevString l_val;
		  d.tga->get_write_value(l_val);

      // set string to upper case
      std::string val = l_val;
      yat::String val_s(val);
      val_s.to_upper();
      m_cfg.pulses.channels[l_id].idleState = val_s;
  		
		  m_pulse->initConfig(m_cfg.pulses);

      // memorize new value
      std::string prop_name = std::string("__") + attrName;
      yat4tango::PropertyHelper::set_property<std::string>(this->m_cfg.hostDevice, prop_name, 
                                                           m_cfg.pulses.channels[l_id].idleState);
    }
	}
	catch (Tango::DevFailed &e)
	{
	  ERROR_STREAM << e << std::endl;
	  m_cfg.pulses.channels[l_id].idleState = l_old_val;
	  RETHROW_DEVFAILED(e, 
      _CPTC("DEVICE_ERROR"), 
		  _CPTC("Idle state update error! Failed to update counter!"), 
		  _CPTC("PulseManager::scalar_attr_idle_wr_cb")); 
  }
  catch (...)
  {
	  ERROR_STREAM << "Idle state update error! Failed to update counter!" << std::endl;
	  m_cfg.pulses.channels[l_id].idleState = l_old_val;
	  THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
		  _CPTC("Idle state update error! Failed to update counter!"), 
		  _CPTC("PulseManager::scalar_attr_idle_wr_cb")); 
	}
}

//============================================================
//PulseManager::scalar_attr_initial_wr_cb
//============================================================
void PulseManager::scalar_attr_initial_wr_cb (yat4tango::DynamicAttributeWriteCallbackData& d)
  throw (Tango::DevFailed)
{
  // write callback for 'initial delay' attribute

	CHECK_PULSE_GENERATOR();

	std::string attrName = d.tga->get_name();

  // check device state
	if (m_state == Tango::RUNNING)
	{
		std::string l_msg = "Writing ";
		l_msg += attrName;
		l_msg += " is not allowed in RUNNING state.";
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC(l_msg.c_str()), 
			_CPTC("PulseGenerationV2::scalar_attr_initial_wr_cb")); 
	}

  // store current value in case of error
	double l_old_val;
	unsigned int l_id=0;

	try
	{
		d.dya->get_user_data<unsigned int>(l_id);

		l_old_val = m_cfg.pulses.channels[l_id].initialDelay;

		Tango::DevDouble l_val;
		d.tga->get_write_value(l_val);
		m_cfg.pulses.channels[l_id].initialDelay = l_val;
		
		m_pulse->initConfig(m_cfg.pulses);

    // memorize new value
    std::string prop_name = std::string("__") + attrName;
    yat4tango::PropertyHelper::set_property<double>(this->m_cfg.hostDevice, prop_name, l_val);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		m_cfg.pulses.channels[l_id].initialDelay = l_old_val;
		RETHROW_DEVFAILED(e, 
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Initial delay update error! Failed to update counter!"), 
			_CPTC("PulseManager::scalar_attr_initial_wr_cb")); 
	}
	catch (...)
	{
		ERROR_STREAM << "Initial delay update error! Failed to update counter!" << std::endl;
		m_cfg.pulses.channels[l_id].initialDelay = l_old_val;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Initial delay update error! Failed to update counter!"), 
			_CPTC("PulseManager::scalar_attr_initial_wr_cb")); 
	}
}

//============================================================
//PulseManager::scalar_attr_low_wr_cb
//============================================================
void PulseManager::scalar_attr_low_wr_cb (yat4tango::DynamicAttributeWriteCallbackData& d)
  throw (Tango::DevFailed)
{
  // write callback for 'low delay' attribute

	CHECK_PULSE_GENERATOR();

	std::string attrName = d.tga->get_name();

  // check device state
	if (m_state == Tango::RUNNING)
	{
		std::string l_msg = "Writing ";
		l_msg += attrName;
		l_msg += " is not allowed in RUNNING state.";
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC(l_msg.c_str()), 
			_CPTC("PulseGenerationV2::scalar_attr_low_wr_cb")); 
	}

  // store current value in case of error
	double l_old_val;
	unsigned int l_id=0;

	try
	{
		d.dya->get_user_data<unsigned int>(l_id);

		l_old_val = m_cfg.pulses.channels[l_id].lowDelay;

		Tango::DevDouble l_val;
		d.tga->get_write_value(l_val);
		m_cfg.pulses.channels[l_id].lowDelay = l_val;
		
    m_pulse->initConfig(m_cfg.pulses);

    // memorize new value
    std::string prop_name = std::string("__") + attrName;
    yat4tango::PropertyHelper::set_property<double>(this->m_cfg.hostDevice, prop_name, l_val);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		m_cfg.pulses.channels[l_id].lowDelay = l_old_val;
		RETHROW_DEVFAILED(e, 
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Low delay update error! Failed to update counter!"), 
			_CPTC("PulseManager::scalar_attr_low_wr_cb")); 
	}
	catch (...)
	{
		ERROR_STREAM << "Low delay update error! Failed to update counter!" << std::endl;
		m_cfg.pulses.channels[l_id].lowDelay = l_old_val;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Low delay update error! Failed to update counter!"), 
			_CPTC("PulseManager::scalar_attr_low_wr_cb")); 
	}
}

//============================================================
//PulseManager::scalar_attr_pulse_wr_cb
//============================================================
void PulseManager::scalar_attr_pulse_wr_cb (yat4tango::DynamicAttributeWriteCallbackData& d)
  throw (Tango::DevFailed)
{
  // write callback for 'pulse width' attribute

	CHECK_PULSE_GENERATOR();

	std::string attrName = d.tga->get_name();

  // check device state
	if (m_state == Tango::RUNNING)
	{
		std::string l_msg = "Writing ";
		l_msg += attrName;
		l_msg += " is not allowed in RUNNING state.";
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC(l_msg.c_str()), 
			_CPTC("PulseGenerationV2::scalar_attr_pulse_wr_cb")); 
	}

  // store current value in case of error
	double l_old_val;
	unsigned int l_id = 0;

	try
	{
		d.dya->get_user_data<unsigned int>(l_id);

		l_old_val = m_cfg.pulses.channels[l_id].highDelay;

		Tango::DevDouble l_val;
		d.tga->get_write_value(l_val);
		m_cfg.pulses.channels[l_id].highDelay = l_val;
		
    m_pulse->initConfig(m_cfg.pulses);

    // memorize new value
    std::string prop_name = std::string("__") + attrName;
    yat4tango::PropertyHelper::set_property<double>(this->m_cfg.hostDevice, prop_name, l_val);
	}
	catch (Tango::DevFailed &e)
	{
		ERROR_STREAM << e << std::endl;
		m_cfg.pulses.channels[l_id].highDelay = l_old_val;
		RETHROW_DEVFAILED(e, 
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Pulse width update error! Failed to update counter!"), 
			_CPTC("PulseManager::scalar_attr_pulse_wr_cb")); 
	}
	catch (...)
	{
		ERROR_STREAM << "Pulse width update error! Failed to update counter!" << std::endl;
		m_cfg.pulses.channels[l_id].highDelay = l_old_val;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Pulse width update error! Failed to update counter!"), 
			_CPTC("PulseManager::scalar_attr_pulse_wr_cb")); 
	}
}

} // namespace PulseGenerationV2_ns

