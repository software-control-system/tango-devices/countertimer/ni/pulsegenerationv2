//=============================================================================
// NI6602_InternalStartTrigger.cpp
//=============================================================================
// abstraction.......NI6602_InternalStartTrigger
// class.............NI6602_InternalStartTrigger
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "NI6602_InternalStartTrigger.h"
#include <yat4tango/ExceptionHelper.h>

namespace PulseGenerationV2_ns
{

	//- check pulse macro:
#define CHECK_START_TRIGGER_PULSE() \
	do \
	{ \
	if (! m_trigPulseTrain) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("The pulse object isn't accessible "), \
	_CPTC("NI6602_InternalStartTrigger::check_pulse")); \
} while (0)


// ============================================================================
// NI6602_InternalStartTrigger::NI6602_InternalStartTrigger ()
// ============================================================================ 
NI6602_InternalStartTrigger::NI6602_InternalStartTrigger (Tango::DeviceImpl * hostDevice)
: yat4tango::TangoLogAdapter(hostDevice)
{
	this->m_trigPulseTrain = NULL;
  this->m_board_id = "";
}

// ============================================================================
// NI6602_InternalStartTrigger::~NI6602_InternalStartTrigger ()
// ============================================================================ 
NI6602_InternalStartTrigger::~NI6602_InternalStartTrigger ()
{
	try
	{
		this->release();
	}
	catch (...)
	{
		// do nothing
	}

	if (this->m_trigPulseTrain != NULL)
	{
		delete m_trigPulseTrain;
		m_trigPulseTrain = NULL;
	}
}

// ============================================================================
// NI6602_InternalStartTrigger::getTriggerData ()
// ============================================================================ 
StartTriggerData NI6602_InternalStartTrigger::getTriggerData() 
  throw (Tango::DevFailed)
{
  StartTriggerData data;

  // Initialize start trigger data (set on ctr7)
  data.edge = TRIGGER_EDGE_RISING;
  data.source = "Ctr7InternalOutput";
  data.external = false;

  return data;
}

// ============================================================================
// NI6602_InternalStartTrigger::init ()
// ============================================================================ 
void NI6602_InternalStartTrigger::init(std::string p_board_id)
  throw (Tango::DevFailed)
{
  // store config
  this->m_board_id = p_board_id;
}

// ============================================================================
// NI6602_InternalStartTrigger::start ()
// ============================================================================ 
void NI6602_InternalStartTrigger::start()
  throw (Tango::DevFailed)
{
  // Delete previous internal start trigger if exists
	if (this->m_trigPulseTrain)
	{
	  try
	  {
      this->m_trigPulseTrain->abort();
		}
		catch(...)
		{
			// DAQmx 9.x abort pb workaround
			WARN_STREAM << "NI6602_InternalStartTrigger::start() - Aborting previous trig pulse generates exception, do nothing..." << std::endl;
		}
    try
    {
	    this->m_trigPulseTrain->release();
	  }
	  catch (...)
	  {
		  // do nothing
      WARN_STREAM << "NI6602_InternalStartTrigger::start() - error while deleting trig pulse train object, do nothing..." << std::endl;
	  }

	  if (this->m_trigPulseTrain != NULL)
	  {
		  delete m_trigPulseTrain;
		  m_trigPulseTrain = NULL;
	  }
	}

  // Create finite pulse train object
	try
	{
		this->m_trigPulseTrain = new ni660Xsl::FinitePulseTrainGeneration();
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to create the start trigger object."), 
			_CPTC("NI6602_InternalStartTrigger::start")); 
	}

	if (!this->m_trigPulseTrain)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to access the start trigger object."), 
			_CPTC("NI6602_InternalStartTrigger::start")); 
	}

  // Initialize trigger data for a start trigger:
  /* idle_state = ni::high
     initial_delay = 0 secs
	   chan_name = /<boardName>/ctr7
	   low_time = 0.1 secs
	   high_time = 0.1 secs
	   nb pulses = 1 */

  ni660Xsl::OutTimeChan chan;
	chan.idle_state = ni::high; //the output is low when no pulse is generated
	chan.initial_delay = 0;
  chan.chan_name = "/" + this->m_board_id + "/ctr7";
	chan.low_time =  0.1; 
	chan.high_time =  0.1;

	m_trigPulseTrain->add_time_channel(chan);
	m_trigPulseTrain->set_nb_pulses(1);

  // Initialize pulse train object
	try
	{
		this->m_trigPulseTrain->init();
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to init pulse object."), 
			_CPTC("NI6602_InternalStartTrigger::start")); 
	}

  // Configure pulse object
	try
	{
		this->m_trigPulseTrain->configure();
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to configure pulse object."), 
			_CPTC("NI6602_InternalStartTrigger::init")); 
	}

	// Start pulse generation
	try
	{
		this->m_trigPulseTrain->start();
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to start start internal trigger."), 
			_CPTC("NI6602_InternalStartTrigger::start")); 
	}
}

// ============================================================================
// NI6602_InternalStartTrigger::stop ()
// ============================================================================ 
void NI6602_InternalStartTrigger::stop()
  throw (Tango::DevFailed)
{
	CHECK_START_TRIGGER_PULSE();

  DEBUG_STREAM << "NI6602_InternalStartTrigger::stop(): entering... !" << endl;

  // Stop pulse generation
	try
	{
		m_trigPulseTrain->stop();
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to stop start trigger."), 
			_CPTC("NI6602_InternalStartTrigger::stop")); 
	}
}

// ============================================================================
// NI6602_InternalStartTrigger::release ()
// ============================================================================ 
void NI6602_InternalStartTrigger::release()
  throw (Tango::DevFailed)
{
	CHECK_START_TRIGGER_PULSE();

  // Release pulse train object
	try
	{
		m_trigPulseTrain->release();
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to release start trigger object."), 
			_CPTC("NI6602_InternalStartTrigger::release")); 
	}
}

} // namespace PulseGenerationV2_ns