//=============================================================================
// NI6602_ContinuousGeneration.h
//=============================================================================
// abstraction.......NI6602_ContinuousGeneration
// class.............NI6602_ContinuousGeneration
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _NI6602_CONTINUOUS_GENERATION_H
#define _NI6602_CONTINUOUS_GENERATION_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "Pulse.h"
#include <yat4tango/LogHelper.h>
#include <NI660Xsl/ContinuousPulseTrainGeneration.h>

namespace PulseGenerationV2_ns
{

// ============================================================================
// class: NI6602_ContinuousGeneration
// ============================================================================
class NI6602_ContinuousGeneration : public Pulse,
                                    public yat4tango::TangoLogAdapter
{

public:

	//- constructor
	NI6602_ContinuousGeneration (Tango::DeviceImpl * hostDevice);

	//- destructor
	virtual ~NI6602_ContinuousGeneration ();

	//- init config
	void initConfig(PulseData p_pulse_def) 
		throw (Tango::DevFailed);

  //- does the pulse mode needs to set a pulse number?
  bool isSetPulseNeeded()
  {
    return false;
  }

	//- start
	void start() 
		throw (Tango::DevFailed);

	//- stop
	void stop() 
		throw (Tango::DevFailed);

	//- release
	void release() 
		throw (Tango::DevFailed);

	//- resetBoard
	void resetBoard() 
		throw (Tango::DevFailed);

	//- getDriverVersion
	std::string getDriverVersion() 
		throw (Tango::DevFailed);

	//- is generation done?
	void isDone(bool &finish) 
		throw (Tango::DevFailed);

	//- gets state
	Tango::DevState get_state();

	//- gets status
	std::string get_status();

protected:
	//- updateCounters_i
	void updateCounters_i() 
		throw (Tango::DevFailed);

private:
  //- set error state
  void set_error_state(std::string msg);

	//- state
	Tango::DevState m_state;

	//- status
	std::string m_status;

	//- NI660Xsl pulse train object
	ni660Xsl::ContinuousPulseTrainGeneration * m_pulseTrain;

	//- pulses definition
	PulseData m_pulse_def;

};

} // namespace PulseGenerationV2_ns

#endif // _NI6602_CONTINUOUS_GENERATION_H
