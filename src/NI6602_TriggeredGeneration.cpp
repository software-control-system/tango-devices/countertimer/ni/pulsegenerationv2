//=============================================================================
// NI6602_TriggeredGeneration.cpp
//=============================================================================
// abstraction.......NI6602_TriggeredGeneration
// class.............NI6602_TriggeredGeneration
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "NI6602_TriggeredGeneration.h"
#include <yat4tango/ExceptionHelper.h>


namespace PulseGenerationV2_ns
{

	//- check macro:
#define CHECK_TRIGGERED_PULSE() \
	do \
	{ \
	if (! m_pulseTrain) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("The pulse object isn't accessible."), \
	_CPTC("NI6602_TriggeredGeneration::check_pulse")); \
} while (0)

// ============================================================================
// NI6602_TriggeredGeneration::NI6602_TriggeredGeneration ()
// ============================================================================ 
NI6602_TriggeredGeneration::NI6602_TriggeredGeneration (Tango::DeviceImpl * hostDevice)
  : yat4tango::TangoLogAdapter(hostDevice)
{
	this->m_pulseTrain = NULL;
  this->m_state = Tango::INIT;
  this->m_status = "Retriggered generation initializing...";
}

// ============================================================================
// NI6602_TriggeredGeneration::~NI6602_TriggeredGeneration ()
// ============================================================================ 
NI6602_TriggeredGeneration::~NI6602_TriggeredGeneration ()
{
	try
	{
		this->release();
	}
	catch (...)
	{
		// do nothing
	}

	if (this->m_pulseTrain)
	{
		delete this->m_pulseTrain;
		this->m_pulseTrain = NULL;
	}
}

// ============================================================================
// NI6602_TriggeredGeneration::initConfig ()
// ============================================================================ 
void NI6602_TriggeredGeneration::initConfig(PulseData p_pulse_def)
  throw (Tango::DevFailed)
{
  // Store config
	this->m_pulse_def	= p_pulse_def;

  // check if external trigger is set
  if (!m_pulse_def.triggerData.external)
	{
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("The extStartTriggerUse attribute must be set to true in RETRIG mode!"), 
			_CPTC("NI6602_TriggeredGeneration::initCounters")); 
	}

  m_state	= Tango::STANDBY;
  m_status = "Retriggered generation init OK.";
}

// ============================================================================
// NI6602_TriggeredGeneration::get_state ()
// ============================================================================ 
Tango::DevState NI6602_TriggeredGeneration::get_state()
{
  if (m_pulseTrain)
  {
	  switch(m_pulseTrain->state())
	  {
	    case ni660Xsl::RetriggerablePulseTrainGeneration::STANDBY:
		    m_status = "Retriggered generation is ready.";
		    m_state	= Tango::STANDBY;
		    break;

	    case ni660Xsl::RetriggerablePulseTrainGeneration::RUNNING:
	    case ni660Xsl::RetriggerablePulseTrainGeneration::INIT:
        // Mask lib INIT state in Device RUNNING state because it's set by lib at generation start
		    m_status = "Retriggered generation is running.";
		    m_state	= Tango::RUNNING;
		    break;

	    default:
		    m_status = "Retriggered generation is in unknown state!";
		    m_state	= Tango::FAULT;
		    break;
	  }
  }
  
  return this->m_state;
}

// ============================================================================
// NI6602_TriggeredGeneration::get_status ()
// ============================================================================ 
std::string NI6602_TriggeredGeneration::get_status()
{
  // compose specific status in RUNNING state
	if ((m_pulse_def.clockType.compare(CLK_INTERNAL) != 0) && 
      (m_state == Tango::RUNNING))
	{
		std::stringstream s;
		s << endl;
		for (ChannelDef_it_t it = m_pulse_def.channels.begin() ; it != m_pulse_def.channels.end(); ++it)
		{
			if (it->second.enabled)
			{
				long delay_ticks = (long)(it->second.lowDelay * m_pulse_def.extClockFreq / 1000.0);
				long pulse_width_ticks = (long)(it->second.highDelay * m_pulse_def.extClockFreq / 1000.0);
				s << "Delay ctr" << it->first << ": " << delay_ticks << " clk ticks; "
					<< "Pulse width ctr" << it->first << ": " << pulse_width_ticks << " clk ticks" << endl;
			}
		}
		m_status += s.str();
	}

	return m_status;
}

// ============================================================================
// NI6602_TriggeredGeneration::start ()
// ============================================================================ 
void NI6602_TriggeredGeneration::start()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "NI6602_TriggeredGeneration::start() entering..." << std::endl;

  // check pulse number value
  if (this->m_pulse_def.nbPulses == 0)
  {
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
      _CPTC("Failed to start retriggered generation: pulse number is null!"), 
			_CPTC("NI6602_TriggeredGeneration::start")); 
  }

  // define counters
	try
	{
		this->updateCounters_i();
	}
	catch (Tango::DevFailed & df)
	{
		ERROR_STREAM << df << std::endl;
		RETHROW_DEVFAILED(df,
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to init RETRIG counters."), 
			_CPTC("NI6602_TriggeredGeneration::start")); 
	}
	catch (...)
	{
		ERROR_STREAM << "Failed to init RETRIG counters - unknown error!" << std::endl;
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to init counters - unknown error!"), 
			_CPTC("NI6602_TriggeredGeneration::start")); 
	}

  // start retriggered pulse train generation
	try
	{
		m_pulseTrain->start();
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to start generation."), 
			_CPTC("NI6602_TriggeredGeneration::start")); 
	}
}

// ============================================================================
// NI6602_TriggeredGeneration::stop ()
// ============================================================================ 
void NI6602_TriggeredGeneration::stop()
  throw (Tango::DevFailed)
{
	CHECK_TRIGGERED_PULSE();

  // stop retriggered pulse train generation
	try
	{
		m_pulseTrain->stop();
	}
	catch (ni660Xsl::DAQException& e)
	{
    ERROR_STREAM << "Failed to stop RETRIG counter NI error" << std::endl;
		throw_devfailed(e);
	}
	catch (...)
	{
    ERROR_STREAM << "Failed to stop RETRIG counters - unknown error!" << std::endl;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to stop generation."), 
			_CPTC("NI6602_TriggeredGeneration::stop")); 
	}
}

// ============================================================================
// NI6602_TriggeredGeneration::updateCounters_i ()
// ============================================================================ 
void NI6602_TriggeredGeneration::updateCounters_i()
  throw (Tango::DevFailed)
{
  DEBUG_STREAM << "NI6602_TriggeredGeneration::updateCounters_i() entering..." << std::endl;

	// check if any counter enabled?
	bool l_chan_pres = false;
	for (ChannelDef_it_t it = m_pulse_def.channels.begin() ; it != m_pulse_def.channels.end(); ++it)
	{
		l_chan_pres |= it->second.enabled;
	}

  // init & configure the pulse train object only if there is at least one counter defined
	if (!l_chan_pres)
	{
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("No counters enabled!"), 
			_CPTC("NI6602_TriggeredGeneration::updateCounters_i")); 
  }

  // check if counters are available according to clock type and number of pulses to generate
	for (ChannelDef_it_t it = m_pulse_def.channels.begin() ; 
       it != m_pulse_def.channels.end(); ++it)
	{
		ChannelDefinition l_def = it->second;

		if (m_pulse_def.nbPulses > 1)
		{
			if ( ((l_def.id == 1) || 
            (l_def.id == 3) || 
            (l_def.id == 5) || 
            (l_def.id == 7)) && l_def.enabled)
			{
				THROW_DEVFAILED(
          _CPTC("CONFIGURATION_ERROR"), 
					_CPTC("Counters 1, 3, 5, 7 not available in RETRIG mode if pulse nb greated than 1."), 
					_CPTC("NI6602_TriggeredGeneration::updateCounters_i")); 
			}
		}
	}

  // check pulse number value
  if (m_pulse_def.nbPulses == 0)
  {
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
      _CPTC("Failed to configure pulse object: pulse number is null!"), 
			_CPTC("NI6602_TriggeredGeneration::updateCounters_i")); 
  }

  // delete current pulse train object if exists
	if (this->m_pulseTrain)
	{
		try
		{
			release();
		}
		catch (...)
		{
      INFO_STREAM << "Failed to release current pulse object." << std::endl;
		}

		delete m_pulseTrain;
		m_pulseTrain	= NULL;
	}

  // create new pulse train object
	try
	{
		m_pulseTrain = new ni660Xsl::RetriggerablePulseTrainGeneration();
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to create the RetriggerablePulseTrainGeneration object."), 
			_CPTC("NI6602_TriggeredGeneration::updateCounters_i")); 
	}

	if (!m_pulseTrain)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to access the RetriggerablePulseTrainGeneration object."), 
			_CPTC("NI6602_TriggeredGeneration::updateCounters_i")); 
	}

  // add counters to pulse train object
	for (ChannelDef_it_t it = m_pulse_def.channels.begin() ; 
       it != m_pulse_def.channels.end(); ++it)
	{
		ChannelDefinition l_def = it->second;

    // Take counter into account only if enabled
    if (l_def.enabled)
		{
      // check idle state
		  ni::LevelType l_type;

		  if (l_def.idleState.compare(IDLE_STATE_LOW) == 0)
		  {
			  l_type = ni::low;
		  } 
		  else if (l_def.idleState.compare(IDLE_STATE_HIGH) == 0)
		  {
			  l_type = ni::high;
		  }
		  else
		  {
        this->set_error_state("Error in counter definition.");

			  THROW_DEVFAILED(
          _CPTC("CONFIGURATION_ERROR"), 
				  _CPTC("IdleState must be LOW or HIGH."), 
				  _CPTC("NI6602_TriggeredGeneration::updateCounters_i")); 
		  }

		  // check clock type
		  if (m_pulse_def.clockType.compare(CLK_INTERNAL) == 0)
		  {
				ni660Xsl::OutTimeChan chan;
				chan.idle_state = l_type;

        // check that high delay value is above min value
	      if (l_def.highDelay < MIN_VALUE_MSECS)
	      {
          this->set_error_state("Error in counter definition.");

          yat::OSStream oss;
          oss << "High delay below min value for counter " << l_def.id << "! Min delay value:" << MIN_VALUE_MSECS << " ms";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("NI6602_TriggeredGeneration::updateCounters_i"));
        }

        // check that low delay value is not null
	      if (l_def.lowDelay < MIN_VALUE_MSECS)
	      {
          this->set_error_state("Error in counter definition.");

          yat::OSStream oss;
          oss << "Low delay below min value for counter " << l_def.id << "! Min delay value:" << MIN_VALUE_MSECS << " ms";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("NI6602_TriggeredGeneration::updateCounters_i"));
        }
				
        stringstream l_stream;
				l_stream << l_def.id;
				chan.chan_name = "/" + m_pulse_def.boardId + CHANNEL_SUFFIX + l_stream.str();
				chan.initial_delay = l_def.initialDelay / 1000.0;//secs
				chan.low_time = l_def.lowDelay / 1000.0;//secs
				chan.high_time = l_def.highDelay / 1000.0;//secs
				
        INFO_STREAM << "configuring " << chan.chan_name << " with:  ";
				INFO_STREAM << "delay" << ": " << l_def.lowDelay << ", ";
        INFO_STREAM << "pulse_width" << ": " << l_def.highDelay << std::endl;

				m_pulseTrain->add_time_channel(chan);
		  }
		  else
		  {
				ni660Xsl::OutClockTicksChan chan;
				chan.idle_state = l_type;

				//convert values in clock ticks
				long initial_delay_ticks = (long)(l_def.initialDelay * m_pulse_def.extClockFreq / 1000.0);
				long delay_ticks = (long)(l_def.lowDelay * m_pulse_def.extClockFreq / 1000.0);
				long pulse_width_ticks = (long)(l_def.highDelay * m_pulse_def.extClockFreq / 1000.0);

        // check that high delay value is above min value
        if (pulse_width_ticks < MIN_VALUE_TICKS)
        {
          this->set_error_state("Error in counter definition.");

          yat::OSStream oss;
          oss << "High delay below min value for counter " << l_def.id << "! Min delay value:" << MIN_VALUE_MSECS << " ms";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("NI6602_TriggeredGeneration::updateCounters_i"));
        }

        // check that low delay value is not null
        if (delay_ticks < MIN_VALUE_TICKS)
        {
          this->set_error_state("Error in counter definition.");

          yat::OSStream oss;
          oss << "Low delay below min value for counter " << l_def.id << "! Min delay value:" << MIN_VALUE_MSECS << " ms";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("NI6602_TriggeredGeneration::updateCounters_i"));
        }

				stringstream l_stream;
				l_stream << l_def.id;
				chan.chan_name = "/" + m_pulse_def.boardId + CHANNEL_SUFFIX + l_stream.str();
				chan.initial_delay = initial_delay_ticks;
				chan.low_ticks = (long) delay_ticks;
				chan.high_ticks = (long) pulse_width_ticks;
				chan.clk_source = "/" + m_pulse_def.boardId + "/"+ m_pulse_def.clockType;
				
        INFO_STREAM << "configuring " << chan.chan_name << " with:  ";
				INFO_STREAM << "delay" << ": " << delay_ticks << ", ";
        INFO_STREAM << "pulse_width" << ": " << pulse_width_ticks << std::endl;

				m_pulseTrain->add_clock_ticks_channel(chan);

        l_def.initialDelay = ((double)(initial_delay_ticks) / m_pulse_def.extClockFreq) * 1000.0;
				l_def.lowDelay = ((double)(delay_ticks) / m_pulse_def.extClockFreq) * 1000.0;
				l_def.highDelay = ((double)(pulse_width_ticks) / m_pulse_def.extClockFreq) * 1000.0;
			}
		}
	} // end for on counters list

  // set start trigger
	ni::EdgeType l_edge = ni::rising_edge;
	if (m_pulse_def.triggerData.edge.compare(TRIGGER_EDGE_FALLING) == 0)
	{
		l_edge = ni::falling_edge;
	} 

	std::string l_str = "/" + m_pulse_def.boardId + "/" + m_pulse_def.triggerData.source;
  DEBUG_STREAM << "Retrig start trigger: " << l_str << endl;
	m_pulseTrain->set_trigger_source(l_str, l_edge);

  // set number of pulses generated at each received trigger
	m_pulseTrain->set_nb_pulses(m_pulse_def.nbPulses);

  // init & configure pulse train object
  try
  {
	  m_pulseTrain->init();
  }
  catch (ni660Xsl::DAQException& e)
  {
    this->set_error_state("Error in counter initialization.");
	  throw_devfailed(e);
  }
  catch (...)
  {
    this->set_error_state("Error in counter initialization.");

	  THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
		  _CPTC("Failed to init pulse object."), 
		  _CPTC("NI6602_TriggeredGeneration::updateCounters_i")); 
  }

	try
	{
		m_pulseTrain->configure();
	}
	catch (ni660Xsl::DAQException& e)
	{
    this->set_error_state("Error in counter configuration.");
		throw_devfailed(e);
	}
	catch (...)
	{
    this->set_error_state("Error in counter configuration.");

		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to configure pulse object."), 
			_CPTC("NI6602_TriggeredGeneration::updateCounters_i")); 
	}
}

// ============================================================================
// NI6602_TriggeredGeneration::resetBoard ()
// ============================================================================ 
void NI6602_TriggeredGeneration::resetBoard()
  throw (Tango::DevFailed)
{
	ni660Xsl::SystemProperties::reset_device(m_pulse_def.boardId);
}

// ============================================================================
// NI6602_TriggeredGeneration::getDriverVersion ()
// ============================================================================ 
std::string NI6602_TriggeredGeneration::getDriverVersion()
  throw (Tango::DevFailed)
{
	std::string l_version;
	l_version = ni660Xsl::SystemProperties::get_driver_version();
	return l_version;
}

// ============================================================================
// NI6602_TriggeredGeneration::isDone ()
// ============================================================================ 
void NI6602_TriggeredGeneration::isDone(bool &p_finish)
throw (Tango::DevFailed)
{
	CHECK_TRIGGERED_PULSE();

  // check if generation id done
	try
	{
		m_pulseTrain->is_done(p_finish);
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to check pulse state."), 
			_CPTC("NI6602_TriggeredGeneration::isDone")); 
	}
}

// ============================================================================
// NI6602_TriggeredGeneration::release ()
// ============================================================================ 
void NI6602_TriggeredGeneration::release()
throw (Tango::DevFailed)
{
	CHECK_TRIGGERED_PULSE();

  // release pulse train object
	try
	{
		m_pulseTrain->release();
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to release pulse object."), 
			_CPTC("NI6602_TriggeredGeneration::release")); 
	}
}

// ============================================================================
// NI6602_TriggeredGeneration::set_error_state ()
// ============================================================================ 
void NI6602_TriggeredGeneration::set_error_state(std::string msg)
{
  // release pulse train object
	try
	{
		this->release();
	}
	catch (...)
	{
		// do nothing
	}

  // delete pulse train object
	if (this->m_pulseTrain)
	{
		delete this->m_pulseTrain;
		this->m_pulseTrain = NULL;
	}
  
  // set internal state to FAULT
  this->m_state = Tango::FAULT;

  // set internal status with msg
  this->m_status = msg;
}

} // namespace PulseGenerationV2_ns