from conan import ConanFile

class PulseGenerationV2Recipe(ConanFile):
    name = "pulsegenerationv2"
    executable = "ds_PulseGenerationV2"
    version = "1.2.1"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Alain Buteau"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/countertimer/ni/pulsegenerationv2.git"
    description = "PulseGenerationV2 device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("nidaqmx/[>=1.0]@soleil/stable")
        self.requires("ni660xsl/[>=1.0]@soleil/stable")
        self.requires("ace/[>=1.0]@soleil/stable")
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
