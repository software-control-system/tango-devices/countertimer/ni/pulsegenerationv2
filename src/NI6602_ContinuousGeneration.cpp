//=============================================================================
// NI6602_ContinuousGeneration.cpp
//=============================================================================
// abstraction.......NI6602_ContinuousGeneration
// class.............NI6602_ContinuousGeneration
// original author...S.Gara - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "NI6602_ContinuousGeneration.h"
#include <yat4tango/ExceptionHelper.h>


namespace PulseGenerationV2_ns
{

	//- check macro:
#define CHECK_CONTINUOUS_PULSE() \
	do \
	{ \
	if (! m_pulseTrain) \
	THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), \
	_CPTC("The train pulse object isn't accessible."), \
	_CPTC("NI6602_ContinuousGeneration::check_pulse")); \
} while (0)

// ============================================================================
// NI6602_ContinuousGeneration::NI6602_ContinuousGeneration ()
// ============================================================================ 
NI6602_ContinuousGeneration::NI6602_ContinuousGeneration (Tango::DeviceImpl * hostDevice)
: yat4tango::TangoLogAdapter(hostDevice)
{
	this->m_pulseTrain = NULL;
  this->m_state = Tango::INIT;
  this->m_status = "Continuous generation initializing...";
}

// ============================================================================
// NI6602_ContinuousGeneration::~NI6602_ContinuousGeneration ()
// ============================================================================ 
NI6602_ContinuousGeneration::~NI6602_ContinuousGeneration ()
{
	try
	{
		this->release();
	}
	catch (...)
	{
		// do nothing
	}

	if (this->m_pulseTrain)
	{
		delete this->m_pulseTrain;
		this->m_pulseTrain = NULL;
	}
}

// ============================================================================
// NI6602_ContinuousGeneration::initConfig ()
// ============================================================================ 
void NI6602_ContinuousGeneration::initConfig(PulseData p_pulse_def)
  throw (Tango::DevFailed)
{
  // store config
	this->m_pulse_def	 = p_pulse_def;

  m_status = "Continuous generation init OK.";
  m_state	= Tango::STANDBY;
}

// ============================================================================
// NI6602_ContinuousGeneration::get_state ()
// ============================================================================ 
Tango::DevState NI6602_ContinuousGeneration::get_state()
{
  if (m_pulseTrain)
  {
	  switch(m_pulseTrain->state())
	  {
      case ni660Xsl::ContinuousPulseTrainGeneration::INIT:
	      m_state	= Tango::INIT;
	      m_status = "Continuous generation is in initialization phase.";
	      break;

      case ni660Xsl::ContinuousPulseTrainGeneration::STANDBY:
	      m_status = "Continuous generation is ready.";
	      m_state	= Tango::STANDBY;
	      break;

      case ni660Xsl::ContinuousPulseTrainGeneration::RUNNING:
	      m_status = "Continuous generation is running.";
	      m_state	= Tango::RUNNING;
	      break;

      default:
	      m_status = "Continuous generation is in an unknown state!";
	      m_state	= Tango::FAULT;
	      break;
	  }
  }

	return this->m_state;
}

// ============================================================================
// NI6602_ContinuousGeneration::get_status ()
// ============================================================================ 
std::string NI6602_ContinuousGeneration::get_status()
{
  // compose specific status in RUNNING state
	if ((m_pulse_def.clockType.compare(CLK_INTERNAL) != 0) && 
      (m_state == Tango::RUNNING))
	{
		std::stringstream s;
		s << endl;

		for (ChannelDef_it_t it = m_pulse_def.channels.begin() ; it != m_pulse_def.channels.end(); ++it)
		{
			if (it->second.enabled)
			{
				long delay_ticks = (long)(it->second.lowDelay * m_pulse_def.extClockFreq / 1000.0);
				long pulse_width_ticks = (long)(it->second.highDelay * m_pulse_def.extClockFreq / 1000.0);
				s << "Delay ctr" << it->first << ": " << delay_ticks << " clk ticks; "
					<< "Pulse width ctr" << it->first << ": " << pulse_width_ticks << " clk ticks" << endl;
			}
		}
		m_status += s.str();
	}

	return m_status;
}

// ============================================================================
// NI6602_ContinuousGeneration::start ()
// ============================================================================ 
void NI6602_ContinuousGeneration::start()
  throw (Tango::DevFailed)
{
  // Define counters
	try
	{
    DEBUG_STREAM << "NI6602_ContinuousGeneration::start() recreate config..." << std::endl;
		this->updateCounters_i();
	}
	catch (Tango::DevFailed & df)
	{
		ERROR_STREAM << df << std::endl;
		RETHROW_DEVFAILED(df,
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to init CONTINUOUS counters."), 
			_CPTC("NI6602_ContinuousGeneration::start")); 
	}
	catch (...)
	{
    ERROR_STREAM << "Failed to init CONTINUOUS counters - unknown error!" << std::endl;
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to init counters - unknown error."), 
			_CPTC("NI6602_ContinuousGeneration::start")); 
	}

  // start continuous pulse train generation
	try
	{
    DEBUG_STREAM << "NI6602_ContinuousGeneration::start() start PULSE..." << std::endl;
		m_pulseTrain->start();
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to start pulse generation."), 
			_CPTC("NI6602_ContinuousGeneration::start")); 
	}
}

// ============================================================================
// NI6602_ContinuousGeneration::stop ()
// ============================================================================ 
void NI6602_ContinuousGeneration::stop()
  throw (Tango::DevFailed)
{
	CHECK_CONTINUOUS_PULSE();

  // stop continuous pulse train generation
	try
	{
    DEBUG_STREAM << "NI6602_ContinuousGeneration::stop(): entering... !" << endl;

		m_pulseTrain->stop();
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to stop pulse generation."), 
			_CPTC("NI6602_ContinuousGeneration::stop")); 
	}
}

// ============================================================================
// NI6602_ContinuousGeneration::updateCounters_i ()
// ============================================================================ 
void NI6602_ContinuousGeneration::updateCounters_i()
  throw (Tango::DevFailed)
{
	// check if any counter enabled?
	bool l_chan_pres = false;
	for (ChannelDef_it_t it = m_pulse_def.channels.begin() ; it != m_pulse_def.channels.end(); ++it)
	{
		l_chan_pres |= it->second.enabled;
	}

  // init & configure the pulse train object only if there is at least one counter defined
	if (!l_chan_pres)
	{
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("No counters enabled!"), 
			_CPTC("NI6602_ContinuousGeneration::updateCounters_i")); 
  }

  // check available counters according to clock type and start trigger type
	for (ChannelDef_it_t it = m_pulse_def.channels.begin() ; 
       it != m_pulse_def.channels.end(); ++it)
	{
		ChannelDefinition l_def = it->second;

    // ctr7 used for internal start trigger
		if ((l_def.id == 7) && 
        !m_pulse_def.triggerData.external && 
        l_def.enabled)
		{
			THROW_DEVFAILED(
        _CPTC("CONFIGURATION_ERROR"), 
				_CPTC("Counter 7 not available in CONTINUOUS mode with internal start trigger."), 
				_CPTC("NI6602_ContinuousGeneration::updateCounters_i")); 
		}
	}


  // delete previous pulse train object if exists
	if (this->m_pulseTrain)
	{
		try
		{
			this->release();
		}
		catch (...)
		{
			INFO_STREAM << "Failed to release the current pulse object." << endl;
		}

		delete this->m_pulseTrain;
		this->m_pulseTrain	= NULL;
	}

  // create new pulse train object
	try
	{
		this->m_pulseTrain = new ni660Xsl::ContinuousPulseTrainGeneration();
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to create the ContinuousPulseTrainGeneration object."), 
			_CPTC("NI6602_ContinuousGeneration::updateCounters_i")); 
	}

	if (!m_pulseTrain)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to access the ContinuousPulseTrainGeneration object."), 
			_CPTC("NI6602_ContinuousGeneration::updateCounters_i")); 
	}


  // add defined counters to pulse train object
	for (ChannelDef_it_t it = m_pulse_def.channels.begin() ; 
       it != m_pulse_def.channels.end(); ++it)
	{
		ChannelDefinition l_def = it->second;

    // Take counter into account only if enabled
		if (l_def.enabled)
		{
      // check idle state
		  ni::LevelType l_type;

		  if (l_def.idleState.compare(IDLE_STATE_LOW) == 0)
		  {
			  l_type = ni::low;
		  } 
		  else if (l_def.idleState.compare(IDLE_STATE_HIGH) == 0)
		  {
			  l_type = ni::high;
		  }
		  else
		  {
        this->set_error_state("Error in counter definition.");

			  THROW_DEVFAILED(
          _CPTC("CONFIGURATION_ERROR"), 
				  _CPTC("IdleState must be LOW or HIGH."), 
				  _CPTC("NI6602_ContinuousGeneration::updateCounters_i")); 
		  }

		  // check clock type
		  if (m_pulse_def.clockType.compare(CLK_INTERNAL) == 0)
		  {
				ni660Xsl::OutTimeChan chan;
				chan.idle_state = l_type;
				
        // check that high delay value is above min value
	      if (l_def.highDelay < MIN_VALUE_MSECS)
	      {
          this->set_error_state("Error in counter definition.");

          yat::OSStream oss;
          oss << "High delay below min value for counter " << l_def.id << "! Min delay value:" << MIN_VALUE_MSECS << " ms";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("NI6602_ContinuousGeneration::updateCounters_i"));
        }

        // check that low delay value is not null
	      if (l_def.lowDelay < MIN_VALUE_MSECS)
	      {
          this->set_error_state("Error in counter definition.");

          yat::OSStream oss;
          oss << "Low delay below min value for counter " << l_def.id << "! Min delay value:" << MIN_VALUE_MSECS << " ms";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("NI6602_ContinuousGeneration::updateCounters_i"));
        }

				stringstream l_stream;
				l_stream << l_def.id;
				chan.chan_name = "/" + m_pulse_def.boardId + CHANNEL_SUFFIX + l_stream.str();
				chan.initial_delay = l_def.initialDelay / 1000.0;//secs
				chan.low_time = l_def.lowDelay / 1000.0;//secs
				chan.high_time = l_def.highDelay / 1000.0;//secs
				
        INFO_STREAM << "configuring " << chan.chan_name << " with:  ";
				INFO_STREAM << "delay" << ": " << l_def.lowDelay << ", ";
        INFO_STREAM << "pulse_width" << ": " << l_def.highDelay << std::endl;

				m_pulseTrain->add_time_channel(chan);
		  }
		  else
		  {
				ni660Xsl::OutClockTicksChan chan;
				chan.idle_state = l_type;

				//convert values in clock ticks
				long initial_delay_ticks = (long)(l_def.initialDelay * m_pulse_def.extClockFreq / 1000.0);
				long delay_ticks = (long)(l_def.lowDelay * m_pulse_def.extClockFreq / 1000.0);
				long pulse_width_ticks = (long)(l_def.highDelay * m_pulse_def.extClockFreq / 1000.0);
				
        // check that high delay value is above min value
        if (pulse_width_ticks < MIN_VALUE_TICKS)
        {
          this->set_error_state("Error in counter definition.");

          yat::OSStream oss;
          oss << "High delay below min value for counter " << l_def.id << "! Min delay value:" << MIN_VALUE_MSECS << " ms";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("NI6602_ContinuousGeneration::updateCounters_i"));
        }

        // check that low delay value is not null
        if (delay_ticks < MIN_VALUE_TICKS)
        {
          this->set_error_state("Error in counter definition.");

          yat::OSStream oss;
          oss << "Low delay below min value for counter " << l_def.id << "! Min delay value:" << MIN_VALUE_MSECS << " ms";
          ERROR_STREAM << oss.str() << std::endl;
          THROW_DEVFAILED(_CPTC("CONFIGURATION_ERROR"), 
                          oss.str().c_str(), 
                          _CPTC("NI6602_ContinuousGeneration::updateCounters_i"));
        }

				stringstream l_stream;
				l_stream << l_def.id;
				chan.chan_name = "/" + m_pulse_def.boardId + CHANNEL_SUFFIX + l_stream.str();
				chan.initial_delay = initial_delay_ticks;
				chan.low_ticks = (long) delay_ticks;
				chan.high_ticks = (long) pulse_width_ticks;
				chan.clk_source = "/" + m_pulse_def.boardId + "/"+  m_pulse_def.clockType;
				
        INFO_STREAM << "configuring " << chan.chan_name << " with:  ";
				INFO_STREAM << "delay" << ": " << delay_ticks << ", ";
        INFO_STREAM << "pulse_width" << ": " << pulse_width_ticks << std::endl;

				m_pulseTrain->add_clock_ticks_channel(chan);

        l_def.initialDelay = ((double)(initial_delay_ticks) / m_pulse_def.extClockFreq) * 1000.0;
				l_def.lowDelay = ((double)(delay_ticks) / m_pulse_def.extClockFreq) * 1000.0;
				l_def.highDelay = ((double)(pulse_width_ticks) / m_pulse_def.extClockFreq) * 1000.0;
			}
		}
	} // end for on counters list
	
  // set start trigger
  ni::EdgeType l_edge = ni::rising_edge;
  if (m_pulse_def.triggerData.edge.compare(TRIGGER_EDGE_FALLING) == 0)
  {
	  l_edge = ni::falling_edge;
  }

	std::string l_str = "/" + m_pulse_def.boardId + "/" + m_pulse_def.triggerData.source;
	DEBUG_STREAM << "Continuous start trigger: " << l_str << endl;
	m_pulseTrain->set_start_trigger(l_str, l_edge);

  // init & configure pulse train object
  try
  {
	  m_pulseTrain->init();
  }
  catch (ni660Xsl::DAQException& e)
  {
    this->set_error_state("Error in counter initialization.");
	  throw_devfailed(e);
  }
  catch (...)
  {
    this->set_error_state("Error in counter initialization.");
	  THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
		  _CPTC("Failed to init pulse object."), 
		  _CPTC("NI6602_ContinuousGeneration::updateCounters_i")); 
  }

	try
	{
		m_pulseTrain->configure();
	}
	catch (ni660Xsl::DAQException& e)
	{
    this->set_error_state("Error in counter configuration.");
		throw_devfailed(e);
	}
	catch (...)
	{
    this->set_error_state("Error in counter configuration.");
		THROW_DEVFAILED(
      _CPTC("CONFIGURATION_ERROR"), 
			_CPTC("Failed to configure pulse object."), 
			_CPTC("NI6602_ContinuousGeneration::updateCounters_i")); 
	}
}

// ============================================================================
// NI6602_ContinuousGeneration::resetBoard ()
// ============================================================================ 
void NI6602_ContinuousGeneration::resetBoard()
  throw (Tango::DevFailed)
{
	ni660Xsl::SystemProperties::reset_device(m_pulse_def.boardId);
}

// ============================================================================
// NI6602_ContinuousGeneration::getDriverVersion ()
// ============================================================================ 
std::string NI6602_ContinuousGeneration::getDriverVersion()
  throw (Tango::DevFailed)
{
	std::string l_version;
	l_version = ni660Xsl::SystemProperties::get_driver_version();
	return l_version;
}

// ============================================================================
// NI6602_TriggeredGeneration::isDone ()
// ============================================================================ 
void NI6602_ContinuousGeneration::isDone(bool &p_finish)
  throw (Tango::DevFailed)
{
	CHECK_CONTINUOUS_PULSE();

  // check if the current generation is done
	try
	{
		m_pulseTrain->is_done(p_finish);
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to check pulse state."), 
			_CPTC("NI6602_ContinuousGeneration::isDone")); 
	}
}

// ============================================================================
// NI6602_ContinuousGeneration::release ()
// ============================================================================ 
void NI6602_ContinuousGeneration::release()
  throw (Tango::DevFailed)
{
	CHECK_CONTINUOUS_PULSE(); 

  // release pulse train object
	try
	{
		this->m_pulseTrain->release();
	}
	catch (ni660Xsl::DAQException& e)
	{
		throw_devfailed(e);
	}
	catch (...)
	{
		THROW_DEVFAILED(
      _CPTC("DEVICE_ERROR"), 
			_CPTC("Failed to release pulse object."), 
			_CPTC("NI6602_ContinuousGeneration::release")); 
	}
}

// ============================================================================
// NI6602_ContinuousGeneration::set_error_state ()
// ============================================================================ 
void NI6602_ContinuousGeneration::set_error_state(std::string msg)
{
  // release pulse train object
	try
	{
		this->release();
	}
	catch (...)
	{
		// do nothing
	}

  // delete pulse train object
	if (this->m_pulseTrain)
	{
		delete this->m_pulseTrain;
		this->m_pulseTrain = NULL;
	}
  
  // set internal state to FAULT
  this->m_state = Tango::FAULT;

  // set internal status with msg
  this->m_status = msg;
}

} // namespace PulseGenerationV2_ns