//=============================================================================
// Pulse.h
//=============================================================================
// abstraction.......Pulse for PulseManager
// class.............Pulse
// original author...S.Gara - Nexeya
//=============================================================================
#pragma once
#ifndef _PULSE_H
#define _PULSE_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "PulseGenerationV2TypesAndConsts.h"


namespace PulseGenerationV2_ns
{

// ============================================================================
// class: Pulse
// ============================================================================
class Pulse
{

public:

  //- destructor
  virtual ~Pulse() {};

  //- set counters configuration
  virtual void initConfig(PulseData p_def) 
    throw (Tango::DevFailed) = 0;

  //- does the pulse mode needs to set a pulse number?
  virtual bool isSetPulseNeeded() = 0;

  //- start
  virtual void start() 
    throw (Tango::DevFailed) = 0;

  //- stop
  virtual void stop() 
    throw (Tango::DevFailed) = 0;

  //- release counters
  virtual void release() 
    throw (Tango::DevFailed) = 0;

  //- reset board
  virtual void resetBoard() 
    throw (Tango::DevFailed) = 0;

  //- get the current driver version
  virtual std::string getDriverVersion() 
    throw (Tango::DevFailed) = 0;

  //- generation is done?
  virtual void isDone(bool &finish) 
    throw (Tango::DevFailed) = 0;

  //- get state
  virtual Tango::DevState get_state() = 0;

  //- get status
  virtual std::string get_status() = 0;
    
protected:

  //- Configure counters
  virtual void updateCounters_i() 
    throw (Tango::DevFailed) = 0;

};

} // namespace PulseGenerationV2_ns

#endif // _PULSE_H
