//=============================================================================
// PulseFactory.cpp
//=============================================================================
// abstraction.......PulseFactory
// class.............PulseFactory
// original author...S.GARA - Nexeya
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================

#include "PulseFactory.h"
#include "NI6602_TriggeredGeneration.h"
#include "NI6602_FiniteGeneration.h"
#include "NI6602_ContinuousGeneration.h"
#include "NI6602_InternalStartTrigger.h"
#include <yat4tango/ExceptionHelper.h>

namespace PulseGenerationV2_ns
{

// ======================================================================
// PulseFactory::instanciate
// ======================================================================
Pulse * PulseFactory::instanciate (Tango::DeviceImpl * hostDevice, BOARD_TYPE p_board, GENERATION_TYPE p_generation)
 throw (Tango::DevFailed)
{
  Pulse * l_pulse = NULL;

  switch (p_board)
  {
  case BT_PXI_6602:

	  switch (p_generation)
	  {
	  case GT_RETRIG:
		  l_pulse = new NI6602_TriggeredGeneration(hostDevice);
		  break;
	  case GT_FINITE:
		  l_pulse = new NI6602_FiniteGeneration(hostDevice);
		  break;
	  case GT_CONTINUOUS:
		  l_pulse = new NI6602_ContinuousGeneration(hostDevice);
		  break;
	  default:
  		THROW_DEVFAILED(
        _CPTC("CONFIGURATION_ERROR"),
        _CPTC("Unknown generation type!"), 
        _CPTC("PulseFactory::instanciate")); 
		  break;		
	  }
	  break;
  default:
  		THROW_DEVFAILED(
        _CPTC("CONFIGURATION_ERROR"),
        _CPTC("Unknown board type!"), 
        _CPTC("PulseFactory::instanciate")); 
	  break;
  }
 
  return l_pulse;
}

// ======================================================================
// PulseFactory::instanciate
// ======================================================================
 InternalStartTrigger * PulseFactory::instanciate (Tango::DeviceImpl * hostDevice, BOARD_TYPE p_board)
   throw (Tango::DevFailed)
{
  InternalStartTrigger * l_trigger = NULL;

  switch (p_board)
  {
  case BT_PXI_6602:
	  l_trigger = new NI6602_InternalStartTrigger(hostDevice);
	  break;
  default:
  		THROW_DEVFAILED(
        _CPTC("CONFIGURATION_ERROR"),
        _CPTC("Unknown board type!"), 
        _CPTC("PulseFactory::instanciate")); 
	  break;
  }
 
  return l_trigger;
}

} // namespace PulseGenerationV2_ns


