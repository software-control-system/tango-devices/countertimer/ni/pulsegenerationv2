//=============================================================================
// InternalStartTrigger.h
//=============================================================================
// abstraction.......InternalStartTrigger for StartTriggerManager
// class.............InternalStartTrigger
// original author...S.Gara - Nexeya
//=============================================================================
#pragma once
#ifndef _INTERNAL_START_TRIGGER_H
#define _INTERNAL_START_TRIGGER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "PulseGenerationV2TypesAndConsts.h"


namespace PulseGenerationV2_ns
{

	// ============================================================================
	// class: InternalStartTrigger
	// ============================================================================
	class InternalStartTrigger 
	{

	public:

    //- destructor
    virtual ~InternalStartTrigger() {};

		//- Init internal start trigger
		virtual void init(std::string p_board_id) 
			throw (Tango::DevFailed) = 0;

		//- Start internal start trigger
		virtual void start() 
			throw (Tango::DevFailed) = 0;

		//- Stop internal start trigger
		virtual void stop() 
			throw (Tango::DevFailed) = 0;

		//- Release internal start trigger
		virtual void release() 
			throw (Tango::DevFailed) = 0;

		//- get trigger data
		virtual StartTriggerData getTriggerData() 
			throw (Tango::DevFailed) = 0;
	};

} // namespace PulseGenerationV2_ns

#endif // _INTERNAL_START_TRIGGER_H
