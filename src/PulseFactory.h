//=============================================================================
// PulseFactory.h
//=============================================================================
// abstraction.......PulseFactory
// class.............PulseFactory
// original author...S.GARA - Nexey
//=============================================================================

#ifndef _PULSE_FACTORY_H_
#define _PULSE_FACTORY_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "Pulse.h"
#include "InternalStartTrigger.h"

namespace PulseGenerationV2_ns
{

// ============================================================================
// class: PulseFactory
// ============================================================================
class PulseFactory
{
public: 
  //- instanciate a specialized pulse object
  static Pulse * instanciate (Tango::DeviceImpl * hostDevice, BOARD_TYPE p_board, GENERATION_TYPE p_generation)
    throw (Tango::DevFailed);
  
  //- instanciate a specialized internal start trigger object
  static InternalStartTrigger * instanciate (Tango::DeviceImpl * hostDevice, BOARD_TYPE p_board)
    throw (Tango::DevFailed);

private:
  PulseFactory ();
  ~PulseFactory ();
};

} // namespace PulseGenerationV2_ns

#endif // _PULSE_FACTORY_H_
