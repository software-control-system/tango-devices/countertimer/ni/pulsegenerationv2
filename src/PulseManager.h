//=============================================================================
// PulseManager.h
//=============================================================================
// abstraction.......PulseManager for PulseGenerationV2
// class.............PulseManager
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _PULSE_MANAGER_H
#define _PULSE_MANAGER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat/time/Timer.h>
#include <yat4tango/DeviceTask.h>
#include <yat4tango/DynamicAttributeManager.h>
#include "PulseGenerationV2TypesAndConsts.h"
#include "PulseFactory.h"
#include "InternalStartTrigger.h"

namespace PulseGenerationV2_ns
{

// ============================================================================
// class: PulseManager
// ============================================================================
class PulseManager :  public yat4tango::DeviceTask
{

public:

  //- constructor
  PulseManager (Tango::DeviceImpl * hostDevice);

  //- destructor
  virtual ~PulseManager ();

  //- gets PulseGenerationV2 manager state
  Tango::DevState get_state();

  //- gets PulseGenerationV2 manager status
  std::string get_status ();

  //- init
  void init(PulseGenerationV2Config& cfg)
	  throw (Tango::DevFailed);

  //- starts the pulse generation
  void start()
	  throw (Tango::DevFailed);

  //- stop the pulse generation
  void stop()
	  throw (Tango::DevFailed);

  //- reset the board
  void resetBoard()
	  throw (Tango::DevFailed);

  //- get driver version
  std::string getDriverVersion()
	  throw (Tango::DevFailed);

  //- update generation type
  void updateGenerationType(std::string p_generation)
	  throw (Tango::DevFailed);

  //- update external start trigger use
  void updateStartTrgUse(bool p_ext_trg, std::string p_ext_src)
	  throw (Tango::DevFailed);
  

protected:
	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg)
		throw (Tango::DevFailed);

private:
  //- updates state and status
  void update_state_status ();

  //- add attribute PulseNb
  void add_attr_pulse_nb()
	  throw (Tango::DevFailed);

  //- remove attribute PulseNb
  void remove_attr_pulse_nb()
	  throw (Tango::DevFailed);
  
  //- configuration
  PulseGenerationV2Config m_cfg;

  //- manage pulse: delete current pulse generator (if exists) & 
  //- create new pulse generator
  void manage_pulse()
	  throw (Tango::DevFailed);


  //- Dynamic attributes management
  bool create_dyn_attrs()
	  throw (Tango::DevFailed);

  //- read callback for attr pulseNb
  void read_pulse_number(yat4tango::DynamicAttributeReadCallbackData & cbd);

  //- write callback for attr pulseNb
  void write_pulse_number(yat4tango::DynamicAttributeWriteCallbackData & cbd);

  //- read callback for attr enabled
  void scalar_attr_enabled_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
	  throw (Tango::DevFailed);

  //- write callback for attr enabled
  void scalar_attr_enabled_wr_cb (yat4tango::DynamicAttributeWriteCallbackData& d)
	  throw (Tango::DevFailed);

  //- read callback for attr idleState
  void scalar_attr_idle_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
	  throw (Tango::DevFailed);

  //- write callback for attr idleState
  void scalar_attr_idle_wr_cb (yat4tango::DynamicAttributeWriteCallbackData& d)
	  throw (Tango::DevFailed);

  //- read callback for attr initialDelay
  void scalar_attr_initial_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
	  throw (Tango::DevFailed);

  //- write callback for attr initialDelay
  void scalar_attr_initial_wr_cb (yat4tango::DynamicAttributeWriteCallbackData& d)
	  throw (Tango::DevFailed);

  //- read callback for attr lowDelay
  void scalar_attr_low_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
	  throw (Tango::DevFailed);

  //- write callback for attr lowDelay
  void scalar_attr_low_wr_cb (yat4tango::DynamicAttributeWriteCallbackData& d)
	  throw (Tango::DevFailed);

  //- read callback for attr pulseWidth
  void scalar_attr_pulse_rd_cb (yat4tango::DynamicAttributeReadCallbackData& d)
	  throw (Tango::DevFailed);

  //- write callback for attr pulseWidth
  void scalar_attr_pulse_wr_cb (yat4tango::DynamicAttributeWriteCallbackData& d)
	  throw (Tango::DevFailed);

  //- dynamic attributes manager
   yat4tango::DynamicAttributeManager * m_dyn_attr_manager;

  //- state
  Tango::DevState m_state;

  //- status
  std::string m_status;

  //- initialization
  void init_i ();
 
  //- periodic job
  void periodic_job_i ()
    throw (Tango::DevFailed);

  //- Pulse generator
  Pulse * m_pulse;

  //- Start Trigger 
  InternalStartTrigger * m_internal_start_trigger;
};

} // namespace PulseGenerationV2_ns

#endif // _PULSE_MANAGER_H
