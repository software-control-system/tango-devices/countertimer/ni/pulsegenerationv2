//=============================================================================
// NI6602_FiniteGeneration.h
//=============================================================================
// abstraction.......NI6602_FiniteGeneration
// class.............NI6602_FiniteGeneration
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _NI6602_FINITE_GENERATION_H
#define _NI6602_FINITE_GENERATION_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "Pulse.h"
#include <yat4tango/LogHelper.h>
#include <NI660Xsl/FinitePulseTrainGeneration.h>


namespace PulseGenerationV2_ns
{

// ============================================================================
// class: NI6602_FiniteGeneration
// ============================================================================
class NI6602_FiniteGeneration : public Pulse,
                                public yat4tango::TangoLogAdapter
{

public:

	//- constructor
	NI6602_FiniteGeneration (Tango::DeviceImpl * hostDevice);

	//- destructor
	virtual ~NI6602_FiniteGeneration ();

	//- init config
	void initConfig(PulseData p_pulse_def) 
		throw (Tango::DevFailed);

  //- does the pulse mode needs to set a pulse number?
  bool isSetPulseNeeded()
  {
    return true;
  }

	//- start
	void start() 
		throw (Tango::DevFailed);

	//- stop
	void stop() 
		throw (Tango::DevFailed);

	//- release
	void release() 
		throw (Tango::DevFailed);

	//- resetBoard
	void resetBoard() 
		throw (Tango::DevFailed);

	//- getDriverVersion
	std::string getDriverVersion() 
		throw (Tango::DevFailed);

	//- isDone
	void isDone(bool &finish) 
		throw (Tango::DevFailed);

	//- gets state
	Tango::DevState get_state();

	//- gets status
	std::string get_status();

protected:
	//- updateCounters_i
	void updateCounters_i() 
		throw (Tango::DevFailed);

private:
  //- set error state
  void set_error_state(std::string msg);

	//- state
	Tango::DevState m_state;

	//- status
	std::string m_status;

	//- NI660Xsl pulse train object
	ni660Xsl::FinitePulseTrainGeneration * m_pulseTrain;

	//- pulses definition
	PulseData m_pulse_def;
};

} // namespace PulseGenerationV2_ns

#endif // _NI6602_FINITE_GENERATION_H
