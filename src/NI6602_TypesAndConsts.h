//=============================================================================
// NI6602TypesAndConsts.h
//=============================================================================
// abstraction.......NI6602 
// class.............NI6602TypesAndConsts
// original author.... S.Gara - NEXEYA
//=============================================================================

#ifndef _NI6602_TYPES_AND_CONSTS_H_
#define _NI6602_TYPES_AND_CONSTS_H_

#pragma once

//=============================================================================
// DEPENDENCIES
//=============================================================================
#include "tango.h"
#include <sstream>
#include <map>

/*Use YAT library*/
#include <yat/memory/DataBuffer.h>
#include <yat/any/Any.h>

//=============================================================================
// IMPL OPTION
//=============================================================================

namespace PulseGenerationV2_ns
{

#define MIN_VALUE_MSECS 0.000025
#define MIN_VALUE_TICKS 2

//-  clocks names
#define CLK_INTERNAL "INTERNAL"

//- channel suffix
#define CHANNEL_SUFFIX "/ctr"

} // namespace PulseGenerationV2_ns

#endif // _NI6602_TYPES_AND_CONSTS_H_