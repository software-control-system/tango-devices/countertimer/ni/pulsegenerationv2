//=============================================================================
// PulseGenerationV2TypesAndConsts.h
//=============================================================================
// abstraction.......PulseGenerationV2 
// class.............PulseGenerationV2TypesAndConsts
// original author.... S.Gara - NEXEYA
//=============================================================================

#ifndef _PULSEGENERATIONV2_TYPES_AND_CONSTS_H_
#define _PULSEGENERATIONV2_TYPES_AND_CONSTS_H_

#pragma once

//=============================================================================
// DEPENDENCIES
//=============================================================================
#include <tango.h>
#include <sstream>
#include <map>

#include <yat/utils/XString.h>
#include <NI660Xsl/SimpleEventCounting.h>

/*Use YAT library*/
#include <yat/memory/DataBuffer.h>
#include <yat/any/Any.h>

#include "NI6602_TypesAndConsts.h"

//=============================================================================
// IMPL OPTION
//=============================================================================

namespace PulseGenerationV2_ns
{

const int SINGLE_DATA = 1;
//- Allowed string delimiters for each property to be parsed
static std::string DELIMS = "\t;|:/";
static size_t MIN_EXPECTED_TOKEN_SCALAR = 8; //- 8 = LABEL + OFFSET + TYPE + MODE + FORMAT + LEVEL + [DESCRIPTION] + [UNIT] + NEXUS + MEMORIZED
static size_t MIN_EXPECTED_TOKEN_SPECTRUM = 5; //- 5 = LABEL + OFFSET + TYPE + LEVEL + [DESCRIPTION]+ [UNIT] + NEXUS
static size_t MIN_EXPECTED_TOKEN_COMMAND = 5; //- 5 = LABEL + OFFSET + TYPEIN + TYPEOUT + LEVEL + [DESCRIPTION]


//- names for dynamic attributes
#define PULSE_GENERATION_TYPE "generationType"
#define EXT_START_TRG_USE "extStartTriggerUse"
#define PULSE_NB "pulseNumber"
#define COUNTER_CH "counter"
#define ENABLED_CH "Enable"
#define IDLE_STATE_CH "idleStateCounter"
#define INITIAL_DELAY_CH "initialDelay"
#define LOW_DELAY_CH "delayCounter"
#define PULSE_WIDTH_CH "pulseWidthCounter"


//- names for others values
#define IDLE_STATE_HIGH "HIGH"
#define IDLE_STATE_LOW "LOW"
#define TRIGGER_EDGE_FALLING "FALLING"
#define TRIGGER_EDGE_RISING "RISING"

//- names for properties
#define IDLESTATE "IdleState"

//- generationType strings
#define GT_CONTINUOUS_STR "CONTINUOUS"
#define GT_FINITE_STR "FINITE"
#define GT_RETRIG_STR "RETRIG"

//- boardType strings
#define BT_PXI_6602_STR "PXI-6602"

//- generationType
typedef enum
{
	GT_UNKNOWN = -1,
	GT_CONTINUOUS = 0,
	GT_FINITE = 1,
	GT_RETRIG = 2
} GENERATION_TYPE;

//- boardType
typedef enum
{
	BT_UNKNOWN = -1,
	BT_PXI_6602 = 0
} BOARD_TYPE;

// ============================================================================
// ChannelDefinition:  struct containing the Channel definition data
// ============================================================================
struct ChannelDefinition
{
	//- name
	std::string name;

	//- enabled ?
	bool enabled;

	//- idle state
	std::string idleState;

	//- initial delay
	double initialDelay;

	//- low delay
	double lowDelay;

	//- high delay
	double highDelay;

	//- id
	unsigned int id;

	ChannelDefinition():
	name(""),
	enabled(false),
	idleState(IDLE_STATE_LOW),
	initialDelay(1.0),
	lowDelay(1.0),
	highDelay(1.0),
	id(0)
	{
	}

	ChannelDefinition (const ChannelDefinition & src)
	{
		*this = src;
	}

	const ChannelDefinition & operator= (const ChannelDefinition & src)
	{
		name = src.name;
		enabled = src.enabled;
		idleState = src.idleState;
		initialDelay = src.initialDelay;
		lowDelay = src.lowDelay;
		highDelay = src.highDelay;
		id = src.id;
		return *this;
	}

	void dump()
	{
		cout << "Name : " << name << endl;
		cout << "Id : " << id << endl;
		cout << "IdleState : " << idleState << endl;
		cout << "Enabled : " << enabled << endl;
		cout << "InitialDelay : " << initialDelay << endl;
		cout << "LowDelay : " << lowDelay << endl;
		cout << "HighDelay : " << highDelay << endl;
	}

};
// ============================================================================
// StartTriggerData:  struct containing start trigger data
// ============================================================================
struct StartTriggerData
{
	//- external trigger ?
	bool external;

	//- edge
	std::string edge;

	//- source
	std::string source;

	StartTriggerData():
	external(false),
	edge(""),
	source("")
	{
	}

	StartTriggerData (const StartTriggerData & src)
	{
		*this = src;
	}

	const StartTriggerData & operator= (const StartTriggerData & src)
	{
		external = src.external;
		edge = src.edge;
		source = src.source;
		return *this;
	}

};

//- map type for storing channel definition - key = counter id
typedef std::map<unsigned int,ChannelDefinition> ChannelDef_t;
typedef ChannelDef_t::iterator ChannelDef_it_t;

// ============================================================================
// PulseData:  struct containing the Pulse definition
// ============================================================================
struct PulseData
{
	//- nb pulses
	unsigned int nbPulses;

	//- clock Type
	std::string clockType;

	//- ext Clock frequency
	double extClockFreq;

	//- channel list
	ChannelDef_t channels;

	//- boardId
	std::string boardId;

	//- start trigger data
	StartTriggerData triggerData;

	PulseData():
	nbPulses(1),
	clockType(""),
	extClockFreq(0.0),
	boardId(""),
	channels(),
	triggerData()
	{
	}

	PulseData (const PulseData & src)
	{
		*this = src;
	}

	const PulseData & operator= (const PulseData & src)
	{
		nbPulses = src.nbPulses;
		clockType = src.clockType;
		boardId = src.boardId;
		extClockFreq = src.extClockFreq	;
		channels = src.channels;
		triggerData = src.triggerData;
		return *this;
	}
};
// ============================================================================
// PulseGenerationV2Config:  struct containing the PulseGenerationV2 configuration
// ============================================================================
typedef struct PulseGenerationV2Config
{
	//- members
	//- host device
	Tango::DeviceImpl * hostDevice;

	//- pulse data
	PulseData pulses;

	//- board type
	BOARD_TYPE boardType;

	//- generation type
	GENERATION_TYPE generationType;

	//- board id
	std::string boardId;

  //- default constructor -----------------------
  PulseGenerationV2Config ()
	  : hostDevice(NULL)
  {
  }

  //- destructor -----------------------
  ~PulseGenerationV2Config ()
  {
  }

  //- copy constructor ------------------
  PulseGenerationV2Config (const PulseGenerationV2Config& src)
  {
    *this = src;
  }

  //- operator= ------------------
  const PulseGenerationV2Config & operator= (const PulseGenerationV2Config& src)
  {
    if (this == & src) 
      return *this;

	  this->hostDevice = src.hostDevice;
	  this->pulses = src.pulses;
	  this->boardType = src.boardType;
	  this->generationType = src.generationType;
	  this->boardId = src.boardId;

    return *this;
  }
      
} PulseGenerationV2Config;

//- ni660Xsl::Exception -> Tango::DevFailed
static void throw_devfailed( ni660Xsl::DAQException& daq_ex )
{
	Tango::DevFailed df;
	const ni660Xsl::ErrorList& daq_errors = daq_ex.errors;
	df.errors.length( daq_errors.size() );
	for (size_t i = 0; i < daq_errors.size(); i++)
	{
		df.errors[i].reason   = CORBA::string_dup( daq_errors[i].reason.c_str() );
		df.errors[i].desc     = CORBA::string_dup( daq_errors[i].desc.c_str()   );
		df.errors[i].origin   = CORBA::string_dup( daq_errors[i].origin.c_str() );
		df.errors[i].severity = static_cast<Tango::ErrSeverity>(daq_errors[i].severity);
	}
	throw df;
}


//- split_property_line
static std::vector<std::string> split_property_line(std::string line)
{
	//std::cout << "\n\t\t LINE TO PARSE \"" << line << "\"" << std::endl;
	//- string iterators
	std::string::size_type begIdx, endIdx;
	std::string tmp("");

	//- result : the splitted line 
	std::vector<std::string> propertyList;

	//- search beginning of the first word
	begIdx = line.find_first_not_of(DELIMS);

	while ( begIdx != std::string::npos )
	{
		//- search the end of the actual word
		endIdx = line.find_first_of(DELIMS, begIdx);
		if ( endIdx == std::string::npos)
		{
			tmp = line.substr(begIdx);
			//std::cout << "\t\t END LINE TO PARSE \"" << tmp << "\"" << std::endl;
			propertyList.push_back( tmp );
			break;
		}

		//- store the property found
		tmp = line.substr(begIdx, (endIdx-begIdx));
		//std::cout << "\t\t LINE PARSED \"" << tmp << "\"\n" << std::endl;
		propertyList.push_back( tmp );

		//- search beginning of the next word
		begIdx = line.find_first_not_of(DELIMS, endIdx);
	}
	return propertyList;
}

} // namespace PulseGenerationV2_ns

#endif // _PULSEGENERATIONV2_TYPES_AND_CONSTS_H_