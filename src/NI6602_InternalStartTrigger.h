//=============================================================================
// NI6602_StartTrigger.h
//=============================================================================
// abstraction.......NI6602_InternalStartTrigger
// class.............NI6602_InternalStartTrigger
// original author...S.Gara - Nexeya
//=============================================================================

#ifndef _NI6602_INTERNAL_START_TRIGGER_H
#define _NI6602_INTERNAL_START_TRIGGER_H

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "InternalStartTrigger.h"
#include <yat4tango/LogHelper.h>
#include <NI660Xsl/FinitePulseTrainGeneration.h>


namespace PulseGenerationV2_ns
{

// ============================================================================
// class: NI6602_InternalStartTrigger
// ============================================================================
class NI6602_InternalStartTrigger : public InternalStartTrigger,
                                    public yat4tango::TangoLogAdapter
{

public:

	//- constructor
	NI6602_InternalStartTrigger (Tango::DeviceImpl * hostDevice);

	//- destructor
	virtual ~NI6602_InternalStartTrigger ();

	//- Init internal start trigger
	virtual void init(std::string p_board_id) 
		throw (Tango::DevFailed);

	//- Start internal start trigger
	virtual void start() 
		throw (Tango::DevFailed);

	//- Stop internal start trigger
	virtual void stop() 
		throw (Tango::DevFailed);

	//- Release internal start trigger
	virtual void release() 
		throw (Tango::DevFailed);

	//- get trigger data (for pulse generation config)
	virtual StartTriggerData getTriggerData() 
		throw (Tango::DevFailed);

private:

	//- Finite pulse train to generate the start signal
	ni660Xsl::FinitePulseTrainGeneration * m_trigPulseTrain;

  std::string m_board_id;
};

} // namespace PulseGenerationV2_ns

#endif // _NI6602_INTERNAL_START_TRIGGER_H
